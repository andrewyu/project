// Do not remove the include below
#include "test23.h"

/*
//The setup function is called once at startup of the sketch
void setup()
{
// Add your initialization code here
	pinMode(13,OUTPUT);
}

// The loop function is called in an endless loop
void loop()
{
//Add your repeated code here
	digitalWrite(13,HIGH);
	delay(1000);
	digitalWrite(13,LOW);
	delay(5000);
}
*/

void pause(){
  digitalWrite(10,LOW);
}

void go(){
  digitalWrite(10,HIGH);
}

void forward(){
  digitalWrite(11, LOW);
  digitalWrite(12, HIGH);
}

void backward(){
  digitalWrite(11, HIGH);
  digitalWrite(12, LOW);
}

void hardbrake(){
  digitalWrite(11,HIGH);
  digitalWrite(12,HIGH);
}

int floor1(){
  return analogRead(17);
}

int floor2(){
  return analogRead(16);
}

int floor3(){
  return analogRead(15);
}

int floor4(){
  return analogRead(14);
}

volatile unsigned char cfloor = 0;

void updateFloor(){
  if(floor1()>800){
    cfloor = 1;
  }
  else if(floor2()>800){
    cfloor = 2;
  }
  else if(floor3()>800){
    cfloor = 3;
  }
  else if(floor4()>800){
    cfloor = 4;
  }
}

void gotofloor(int targetFloor){
  if(targetFloor > cfloor){
    backward();
  }
  else{
    forward();
  }

  while(targetFloor != cfloor){
    updateFloor();
    go();
  }
  pause();
}



void setup()
{
  Serial.begin(9600);
  pinMode(10, OUTPUT);
  pinMode(11, OUTPUT);
  pinMode(12, OUTPUT);
  digitalWrite(10, LOW);
  pinMode(14,INPUT);
  pinMode(15,INPUT);
  pinMode(16,INPUT);
  pinMode(17,INPUT);

  backward();
  go();
  delay(3000);
  pause();
  //delay(3000);
}





void loop()
{
  //pause();
  forward();
  go();
  Serial.println("forward");

  while(floor4()>800);


  hardbrake();
  Serial.println("stop");

  delay(300);

  backward();
  go();
  Serial.println("back");


  while(floor2()>800);

  hardbrake();
  delay(300);
}
//Serial.println(cfloor);

/*
gotofloor(4);
delay(1000);
gotofloor(2);
delay(1000);
gotofloor(1);
delay(1000);
*/
