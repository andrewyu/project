//hellotest
#include <Wire.h>
#include <Time.h>  
#define eep1 0x50
#define firstpos 0

void logEntry(time_t time, byte user);
void setup() 
{
  Wire.begin();
  Serial.begin(115200);
  unsigned int address = 0;
  writeEEPROM(eep1, address, 0);
  writeEEPROM(eep1, address+1, 0);
  ////Serial.print(readEEPROM(eep1, address), DEC);
  delay(5000);
  logEntry(Teensy3Clock.get(), 0xAA);
}

void loop(){
 
  /*
  Wire.beginTransmission(eep1);
  Wire.send((int)(0 >> 8));   // MSB
  Wire.send((int)(0 & 0xFF)); // LSB
  Wire.send(0xAB);
  Wire.send(0xBC);//combine
  Wire.endTransmission();
 
  delay(5);
  */
  
  Wire.beginTransmission(eep1);
  Wire.send((int)(2 >> 8));   // MSB
  Wire.send((int)(2 & 0xFF)); // LSB
  Wire.endTransmission();
  Wire.requestFrom(eep1, 5);    // request 6 bytes from slave device #2
  while(Wire.available())    // slave may send less than requested
  { 
    char c = Wire.read();    // receive a byte as character
    Serial.print(c,HEX);         // print the character
  }
  delay(5000);
  
}

void writeEEPROM(int deviceaddress, unsigned int eeaddress, byte data ) 
{
  Wire.beginTransmission(deviceaddress);
  Wire.send((int)(eeaddress >> 8));   // MSB
  Wire.send((int)(eeaddress & 0xFF)); // LSB
  Wire.send(data);
  Wire.endTransmission();
 
  delay(5);
}
 
byte readEEPROM(int deviceaddress, unsigned int eeaddress ) 
{
  byte rdata = 0xFF;
 
  Wire.beginTransmission(deviceaddress);
  Wire.send((int)(eeaddress >> 8));   // MSB
  Wire.send((int)(eeaddress & 0xFF)); // LSB
  Wire.endTransmission();
 
  Wire.requestFrom(deviceaddress,1);
 
  if (Wire.available()) rdata = Wire.receive();
 
  return rdata;
}

// Note on EEPROM
// Only one EEPROM is used, so device address is hard coded
// Data is structured so that address 0 contains # of entries
// each entries uses 4 bytes for time,
// 1 byte for type of entry/user
// 11111100 door open  
// 11111101 door close 
// 6bit usercode + 10 door unlocked
// 11111111 door locked

void logEntry(time_t t, byte user){ 
  // get num entries, calculate writing pos, write 't' and 'user',
  // increment num entries
  
  elapsedMicros sinceStart; //testing; time execution
  byte data = 0x00;
  int entries = 0;
  // Get num entries stored 1st posistion //
  Wire.beginTransmission(eep1); 
  Wire.send((int)(firstpos >> 8));   // MSB
  Wire.send((int)(firstpos & 0xFF)); // LSB
  Wire.endTransmission();
  delay(5);
  Wire.requestFrom(eep1,2);
  if (Wire.available()) {
    data = Wire.receive();
  }
  entries = data<<8; //upper byte
  if (Wire.available()) {
    data = Wire.receive();
  }
  entries = entries | data; //lower byte
  Serial.println(entries);
  Serial.println((int)sinceStart);
  
  //==// send data
  byte beginAddress = (entries*5)+2;
  Wire.beginTransmission(eep1);
  Wire.send((int)(beginAddress >> 8));   // MSB
  Wire.send((int)(beginAddress & 0xFF)); // LSB
  Wire.send( (int)(t & 0x000000FF) );
  Wire.send( (int)((t>>8)&0x000000FF) );
  Wire.send( (int)((t>>16)&0x000000FF) );
  Wire.send( (int)((t>>24)&0x000000FF) );
  Wire.send( user );
  Wire.endTransmission();
  delay(5);
  Serial.println("HERE");
  //==// update num entries
  Wire.beginTransmission(eep1);
  Wire.send((int)(firstpos >> 8));   // MSB
  Wire.send((int)(firstpos & 0xFF)); // LSB
  Wire.send( (((entries + 1)>>8)&0x00FF) );
  Wire.send( ((entries + 1)&0x00FF) );
  Wire.endTransmission();
  delay(5);
  Serial.println((int)sinceStart);
}
