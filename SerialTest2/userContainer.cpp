/*
 * userContainer.cpp
 *
 *  Created on: Apr 2, 2014
 *      Author: Computer
 */

#include "userContainer.h"

void printDigits2(int digits){
	// utility function for digital clock display: prints preceding colon and leading 0

	if(digits < 10)
		Serial.print('0');
	Serial.print(digits);

}
void digitalClockDisplay2(time_t t) {
	// digital clock display of the time
	printDigits2(hour(t));
	Serial.print(":");
	printDigits2(minute(t));
	Serial.print(":");
	printDigits2(second(t));
	Serial.print(',');
	printDigits2(month(t));
	Serial.print("/");
	printDigits2(day(t));
	Serial.print("/");
	Serial.print(year(t));
	//	Serial.println();
	//Serial.write(10);
}
void printDigits3(int digits){
	// utility function for digital clock display: prints preceding colon and leading 0

	if(digits < 10)
		Serial3.print('0');
	Serial3.print(digits);

}
void digitalClockDisplay3(time_t t) {
	// digital clock display of the time
	printDigits3(hour(t));
	Serial3.print(":");
	printDigits3(minute(t));
	Serial3.print(":");
	printDigits3(second(t));
	Serial3.print(',');
	printDigits3(month(t));
	Serial3.print("/");
	printDigits3(day(t));
	Serial3.print("/");
	Serial3.print(year(t));
	//	Serial.println();
	//Serial.write(10);

}


String removeExtraSpace(String input){ //Works but no longer needed

	input.replace(13,10);
	int tempInd10 = input.indexOf(10);
	int length = input.length();

	Serial.println(tempInd10);
	Serial.println(length);



	while( tempInd10 != -1 ){
		if(tempInd10 == 0){
			input = input.substring(1);

			char temp[10];
			input.toCharArray(temp,10);
			//print(temp);
		}
		else{
			String t1 = input.substring(0,tempInd10);
			String t2 = input.substring(tempInd10+1);
			input = String(t1 + t2);

			char temp[10];
			input.toCharArray(temp,10);
			//print(temp);
		}
		if(tempInd10 == input.length()-1){
			input.replace(10,0);
			Serial.print("last");

			char temp[10];
			input.toCharArray(temp,10);
			//print(temp);
		}

		tempInd10 = input.indexOf(10);
		Serial.println(tempInd10);
		Serial.println(input.length());
	}

	return input;
}



userContainer::userContainer() {
	_numUsers = 0;
}

void userContainer::addUser(User toAdd){
	if(_numUsers >=63){
		return;
	}
	_arrayofUsers[_numUsers]=toAdd;
	_numUsers++;
	backupToEEPROM();
}

int userContainer::getNumUsers(){
	return _numUsers;
}

int userContainer::getUserPos(String userId){
	int i;
	for(i=0;i<_numUsers;i++){
		if(userId.equals(_arrayofUsers[i].getUserId())){
			return i;
		}
	}
	return -1;
}

int userContainer::getLogId(int pos){
	return _arrayofUsers[pos].getLogId();
}

String userContainer::getUserId(int pos){
	return _arrayofUsers[pos].getUserId();
}

String userContainer::getAlias(int pos){
	return _arrayofUsers[pos].getAlias();
}

int userContainer::isAllowed(int pos){
	return _arrayofUsers[pos].isAllowed();
}

void userContainer::printUserInfo(int pos){
	Serial.print("User number: ");
	Serial.println(pos+1);
	Serial.print("User ID: ");
	Serial.println(_arrayofUsers[pos].getUserId());
	Serial.print("Log ID: ");
	Serial.println(_arrayofUsers[pos].getLogId());
	Serial.print("Alias: ");
	Serial.println(_arrayofUsers[pos].getAlias());
	Serial.print("Allowed time Start: ");
	digitalClockDisplay2(_arrayofUsers[pos].getStartTime());
	Serial.println("");
	Serial.print("Allowed time End: ");
	digitalClockDisplay2(_arrayofUsers[pos].getEndTime());;
	Serial.println('\n');
}
void userContainer::printUserInfoToWifi(int pos){

	Serial3.print("User number: ");
	Serial3.println(pos+1);
	Serial3.print("User ID: ");
	Serial3.println(_arrayofUsers[pos].getUserId());
	Serial3.print("Log ID: ");
	Serial3.println(_arrayofUsers[pos].getLogId());
	Serial3.print("Alias: ");
	Serial3.println(_arrayofUsers[pos].getAlias());
	Serial3.print("Allowed time Start: ");
	digitalClockDisplay3(_arrayofUsers[pos].getStartTime());
	Serial3.println("");
	Serial3.print("Allowed time End: ");
	digitalClockDisplay3(_arrayofUsers[pos].getEndTime());;
	Serial3.println('\n');

}

void userContainer::printUserInfoAll(){
	int i;
	for(i=0;i<_numUsers;i++){
		//		Serial.print("User number: ");
		//		Serial.println(i+1);
		//		Serial.print("User ID: ");
		//		Serial.println(_arrayofUsers[i].getUserId());
		//		Serial.print("Log ID: ");
		//		Serial.println(_arrayofUsers[i].getLogId());
		//		Serial.print("Alias: ");
		//		Serial.println(_arrayofUsers[i].getAlias());
		//		Serial.print("Allowed time Start: ");
		//		digitalClockDisplay2(_arrayofUsers[i].getStartTime());
		//		Serial.println("");
		//		Serial.print("Allowed time End: ");
		//		digitalClockDisplay2(_arrayofUsers[i].getEndTime());;
		//		Serial.println('\n');
		printUserInfo(i);
	}
}

void userContainer::printUserInfoAllWifi(){
	int i;
	Serial3.print('|');
	for(i=0;i<_numUsers;i++){

		printUserInfoToWifi(i);
	}
	Serial3.print('~');
}

void userContainer::disAllowUser(int pos){
	_arrayofUsers[pos].disallow();
	backupToEEPROM();
}

void userContainer::allowBTuser(int i){
	String uid = _arrayofUsers[i].getUserId();
	if(uid.indexOf('-')==0){
		String newUserID = uid.substring(1);
		_arrayofUsers[i].setUserID(removeExtraSpace(newUserID));
	}
	backupToEEPROM();
}

void userContainer::clearDisallowed(){
	int oldindex = 0, newindex = 0;
	int newnumusers=0;
	User temp[64];

	for( oldindex = 0; oldindex < _numUsers; oldindex++){
		if(_arrayofUsers[oldindex].getEndTime() > Teensy3Clock.get() ){
			temp[newindex]=_arrayofUsers[oldindex];
			temp[newindex].setLogId(newindex);
			newindex++;
			newnumusers++;
		}
	}
	int i = 0;
	for(i=0;i<newnumusers;i++){
		_arrayofUsers[i] = temp[i];
	}
	//_arrayofUsers = temp;
	_numUsers = newnumusers;
	backupToEEPROM();
}

void userContainer::editUser(int logid, time_t timestart, time_t timeend, String alias){
	_arrayofUsers[logid].changeStartTime(timestart);
	_arrayofUsers[logid].changeEndTime(timeend);
	_arrayofUsers[logid].setAlias(alias);
	backupToEEPROM();
}

void userContainer::backupToEEPROM(){
	int i, eeposition=0;
	int it=0;

	EEPROM.write(eeposition,_numUsers);
	eeposition++;

	for(i=0; i<_numUsers; i++){
		EEPROM.write(eeposition, _arrayofUsers[i].getLogId());
		eeposition++;
		time_t starttime = _arrayofUsers[i].getStartTime();
		EEPROM.write(eeposition, ((uint8_t)(starttime >> 24) & 0x00FF) );
		eeposition++;
		EEPROM.write(eeposition, ((uint8_t)(starttime >> 16) & 0x00FF) );
		eeposition++;
		EEPROM.write(eeposition, ((uint8_t)(starttime >> 8) & 0x00FF) );
		eeposition++;
		EEPROM.write(eeposition, ((uint8_t)(starttime ) & 0x00FF) );
		eeposition++;
		time_t endtime = _arrayofUsers[i].getEndTime();
		EEPROM.write(eeposition, ((uint8_t)(endtime >> 24) & 0x00FF) );
		eeposition++;
		EEPROM.write(eeposition, ((uint8_t)(endtime >> 16) & 0x00FF) );
		eeposition++;
		EEPROM.write(eeposition, ((uint8_t)(endtime >> 8) & 0x00FF) );
		eeposition++;
		EEPROM.write(eeposition, ((uint8_t)(endtime ) & 0x00FF) );
		eeposition++;
		String userId = _arrayofUsers[i].getUserId();
		EEPROM.write(eeposition, userId.length());
		eeposition++;
		for(it=0;it<userId.length();it++){
			EEPROM.write(eeposition, (uint8_t)userId.charAt(it));
			eeposition++;
		}
		String alias = _arrayofUsers[i].getAlias();
		EEPROM.write(eeposition, alias.length());
		eeposition++;
		for(it=0;it<alias.length();it++){
			EEPROM.write(eeposition, (uint8_t)alias.charAt(it));
			eeposition++;
		}
	}
}

void userContainer::loadFromEEPROM(){
	//Serial.println("STARTING");

	User tempuserarr[64];
	int numusers, logid, i ,position=0, useridsize, aliassize, tempindex;
	time_t tstart=0, tend=0;
	String userid, alias;

	numusers = (int)EEPROM.read(position);
	position++;

	//Serial.println(numusers);

	for(i=0; i<numusers; i++){
		logid = (int)EEPROM.read(position);
		position++;

		tstart = 0;
		tstart |= (time_t)(EEPROM.read(position)<<24);
		position++;
		tstart |= (time_t)(EEPROM.read(position)<<16);
		position++;
		tstart |= (time_t)(EEPROM.read(position)<<8);
		position++;
		tstart |= (time_t)(EEPROM.read(position));
		position++;
		//digitalClockDisplay2(tstart);
		//Serial.println("");
		tend = 0;
		tend |= (time_t)(EEPROM.read(position)<<24);
		position++;
		tend |= (time_t)(EEPROM.read(position)<<16);
		position++;
		tend |= (time_t)(EEPROM.read(position)<<8);
		position++;
		tend |= (time_t)(EEPROM.read(position));
		position++;
		//digitalClockDisplay2(tend);
		//Serial.println("");

		useridsize = (int)EEPROM.read(position);
		position++;
		//Serial.println(useridsize);
		char tempstr[60];
		for(tempindex = 0; tempindex<useridsize; tempindex++){
			tempstr[tempindex] = EEPROM.read(position);
			position++;
		}
		userid = String(tempstr);
		//Serial.print(userid);

		memset(tempstr,0,60);

		aliassize = (int)EEPROM.read(position);
		position++;
		for(tempindex = 0; tempindex<aliassize; tempindex++){
			tempstr[tempindex] = EEPROM.read(position);
			position++;
		}
		alias = String(tempstr);
		memset(tempstr,0,60);

		User temp(userid, logid, tstart, tend, alias);
		tempuserarr[i] = temp;

		//Serial.print(userid);
	}

	for(tempindex = 0; tempindex<numusers; tempindex++){
		_arrayofUsers[tempindex] = tempuserarr[tempindex];
	}
	_numUsers = numusers;

}

userContainer::~userContainer() {
	// TODO Auto-generated destructor stub
}

