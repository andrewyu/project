// Do not remove the include below
#include "SerniorDesign_code.h"
//#include <utility/Key.h>
//#include "Keypad.h"
//#include <Keypad.h>
#include <Wire.h>
#include <Time.h>
#define eep1 0x50
#define firstpos 0
#define connected 1
#define disconnected 0
#define verboseon 1
#define kpad 0
#define serial 1
#define bluetooth 2
#define wifi 3


typedef struct Entry{
	time_t time;
	byte user;
	byte entryType;
} Entry;
void logEntry(time_t time, byte user, byte type);
void writeEEPROM(int deviceaddress, unsigned int eeaddress, byte data ) ;
void setAddress(int deviceAddress, unsigned int address);
byte readEEPROM(int deviceaddress, unsigned int eeaddress );
void dispLog();
void dispLogBT();
void dispLogWIFI();
void printDigits(int digits);
void digitalClockDisplay(time_t t);
void printDigits2(int digits);
void digitalClockDisplay2(time_t t);
void printDigits3(int digits);
void digitalClockDisplay3(time_t t); //TODO: combine bt and wifi log sending into one function
Entry getEntry(int entNum);
time_t tmConvert_t(int YYYY, byte MM, byte DD, byte hh, byte mm, byte ss);
int extract(char *ret, char *arr, int pos);
void print( char *arr);
time_t getTeensy3Time();
byte strCmp(char * arr, char * compto);
void chara2uinta(uint8_t *returnarr, char* inarr);
int extractall(char *ret, char *arr, int pos);
int commandSentBy;
void btsend(char arr[], uint8_t size){
	uint8_t sendarr[size];
	chara2uinta(sendarr, arr);
	Serial2.write(sendarr,size);
	Serial2.write(0xD);
}
void lockDoor();
void unlockDoor();

void setup()
{
	//Teensy3Clock.set((time_t)(int)1393604046);
	setSyncProvider(getTeensy3Time);
	Wire.begin();
	Serial.begin(115200);
	Serial2.begin(115200);
	Serial3.begin(19200);
	unsigned int address = 0;
	//writeEEPROM(eep1, address, 0);
	//writeEEPROM(eep1, address+1, 0);
	////Serial.print(readEEPROM(eep1, address), DEC);
	delay(5000);
	Serial.println("Program Start");
	//logEntry((time_t)0x00000000,0, 0);
	pinMode(13,OUTPUT);
	pinMode(15,OUTPUT);
	pinMode(16,OUTPUT);
	pinMode(17,OUTPUT);
}



char testarr[] = "nosettime,14,02,28,02,29,50";
//void synctime(char arr[]){

char receiverarray[50];
char receiver2array[50];
char command[20];
byte BTconStatus = disconnected;

void loop(){
  char customKey = customKeypad.getKey();

  if (customKey){
    Serial.println(customKey);
  }

	//dispLog();
	if(Serial.available()){
		Serial.readBytesUntil(10,receiverarray,50);
		commandSentBy = serial;
	}
	else if(Serial2.available()){
		Serial2.readBytesUntil(0xD,receiverarray,50);
		print(receiverarray);
		commandSentBy = bluetooth;
	}
	else if(Serial3.available()){
		Serial3.readBytesUntil(0xD,receiverarray,50);
		print(receiverarray);
		commandSentBy = wifi;
	}
	else{
		commandSentBy = -1;
	}


	if(commandSentBy){
		switch(commandSentBy){
		case serial:
			Serial.println("Command sent by serial");
			break;
		case bluetooth:
			Serial.println("Command sent by bluetooth");
			break;
		case wifi:
			Serial.println("Command sent by wifi");
			break;
		default:
			break;
		}
	}

	int sizee = extract(command,receiverarray, 0); //use size of command for switch
	if(strCmp(command,"CONNECT")&&(commandSentBy==bluetooth)){//make another array for serial2
		BTconStatus = connected;
		memset(command,0,20);
		memset(receiverarray,0,50);
	}
	else if(strCmp(command,"DISCONNECT")&&(commandSentBy==bluetooth)){
		BTconStatus = disconnected;
		memset(command,0,20);
		memset(receiverarray,0,50);
	}
	//int size2 = extract(command, receiverarray, 0);

	if(sizee){
		Serial.print("Command size: ");
		Serial.println(sizee);
	}


	if(strCmp(command,"settime")){
		char t[2];
		int yr;
		byte MM,DD,hh,mm,ss;
		extract(t,receiverarray,1);
		yr = 2000 + (int)((t[0]-48)*10) + (int)(t[1]-48);
		extract(t,receiverarray,2);
		MM = (int)((t[0]-48)*10) + (int)(t[1]-48);
		extract(t,receiverarray,3);
		DD = (int)((t[0]-48)*10) + (int)(t[1]-48);
		extract(t,receiverarray,4);
		hh = (int)((t[0]-48)*10) + (int)(t[1]-48);
		extract(t,receiverarray,5);
		mm = (int)((t[0]-48)*10) + (int)(t[1]-48);
		extract(t,receiverarray,6);
		ss = (int)((t[0]-48)*10) + (int)(t[1]-48);
		Teensy3Clock.set(tmConvert_t(yr,MM,DD,hh,mm,ss));
		memset(command,0,20);
		memset(receiverarray,0,50);
	}

	if(strCmp(command,"lognow")){
		Serial.println("lognow");
		logEntry(getTeensy3Time(), 1,1);
		memset(command,0,20);
		memset(receiverarray,0,50);
	}

	if(strCmp(command,"displaylog?")){
		Serial.println("displaylog");
		switch(commandSentBy){
		case serial:
			dispLog();
			break;
		case bluetooth:
			dispLogBT();
			break;
		case wifi:
			dispLogWIFI();
			break;
		default:
			Serial.println("displaylogError"); //should never happen
			break;
		}
		//dispLog();
		//dispLogBT();
		memset(command,0,20);
		memset(receiverarray,0,50);
	}

	if(strCmp(command,"time?")){
		digitalClockDisplay(Teensy3Clock.get());
		memset(command,0,20);
		memset(receiverarray,0,50);
	}
	char arrtest[] = "abcdefg";
	if(strCmp(command,"BTstatus?")){
		if(BTconStatus == connected){
			Serial.println("BT is connected");
			btsend(arrtest,7);
		}
		else{
			Serial.println("BT is Disconnected");
		}
		memset(command,0,20);
		memset(receiverarray,0,50);
	}

	if(strCmp(command,"BTsend")){
		char test[20];
		int size = extractall(test,receiverarray,1);
		uint8_t sendarr[size];
		chara2uinta(sendarr, test);
		Serial2.write(sendarr,size);
		Serial2.write(0xD);
		memset(command,0,20);
		memset(receiverarray,0,50);
	}
	if(strCmp(command,"led")){
		digitalWrite(13,!digitalRead(13));
	}
	if(strCmp(command,"WIFIsend")){
		Serial.print("sending");
		char test[20];
		int size = extractall(test,receiverarray,1);
		uint8_t sendarr[size];
		chara2uinta(sendarr, test);
		Serial3.write('|');
		Serial3.write(sendarr,size);
		Serial3.write('~');

		//				memset(command,0,20);
		//				memset(receiverarray,0,50);

		//		time_t now = getTeensy3Time();
		//		Serial3.write('|');
		//		Serial3.print((int)now);
		//		Serial3.write('~');
		memset(command,0,20);
		memset(receiverarray,0,50);
	}

	if(strCmp(command,"getRSSI")){
		int rssi;
		char rssiarr[10];
		if(BTconStatus==disconnected){
			rssi = -1;
		}
		uint8_t mes[] = "ATRSSI?,0";
		Serial2.write("+++");
		Serial2.write(0xD);
		delay(100);
		Serial2.write(mes,9);
		Serial2.write(0xD);
		while(Serial2.available());
		Serial2.readBytesUntil(0xD,rssiarr,10);
		int ind = 0;
		while(rssiarr[ind]!='-'){
			ind++;
		}
		rssi = (int)((rssiarr[ind+1]-48)*100) + (int)((rssiarr[ind+2]-48)*10) + (int)((rssiarr[ind+3]-48));
		Serial.print(rssi);
		memset(command,0,20);
		memset(receiverarray,0,50);
	}

	if(strCmp(command,"cmode")){
		Serial2.print("+++");
		Serial2.write(0xd);
		memset(command,0,20);
		memset(receiverarray,0,50);
	}
	if(strCmp(command,"dcmode")){
		Serial2.print("atmd");
		Serial2.write(0xd);
		memset(command,0,20);
		memset(receiverarray,0,50);
	}
	if(strCmp(command,"doorlock")){
		lockDoor();
		memset(command,0,20);
		memset(receiverarray,0,50);
	}
	if(strCmp(command,"doorunlock")){
			unlockDoor();
			memset(command,0,20);
			memset(receiverarray,0,50);
	}
	if(strCmp(command,"clear!")){
				memset(command,0,20);
				memset(receiverarray,0,50);
		}
	memset(command,0,20);
	memset(receiverarray,0,50);
	memset(receiver2array,0,50);
	delay(50);
}

void writeEEPROM(int deviceaddress, unsigned int eeaddress, byte data )
{
	Wire.beginTransmission(deviceaddress);
	Wire.send((int)(eeaddress >> 8));   // MSB
	Wire.send((int)(eeaddress & 0xFF)); // LSB
	Wire.send(data);
	Wire.endTransmission();
	delay(5);
}

byte readEEPROM(int deviceaddress, unsigned int eeaddress )
{
	byte rdata = 0xFF;
	Wire.beginTransmission(deviceaddress);
	Wire.send((int)(eeaddress >> 8));   // MSB
	Wire.send((int)(eeaddress & 0xFF)); // LSB
	Wire.endTransmission();

	Wire.requestFrom(deviceaddress,1);

	if (Wire.available()) rdata = Wire.receive();

	return rdata;
}

// Note on EEPROM
// Only one EEPROM is used, so device address is hard coded
// Data is structured so that address 0 contains # of entries
// each entries uses 4 bytes for time,
// 1 byte for type of entry/user
// 11111100 door open
// 11111101 door close
// 6bit usercode + 10 door unlocked
// 11111111 door locked
void logEntry(time_t t, byte user, byte type){
	// get num entries, calculate writing pos, write 't' and 'user',
	// increment num entries
	//Serial.print((int)t,DEC);
	elapsedMicros sinceStart; //testing; time execution
	byte data = 0x00;
	int entries = 0;
	// Get num entries stored 1st posistion //
	data = readEEPROM(eep1,0);
	entries = data<<8; //upper byte
	data = readEEPROM(eep1,1);
	entries = entries | data; //lower byte
	if(verboseon){
		Serial.print(entries);
		Serial.println(" entries previously stored.");
	}
	//==debug==Serial.println((int)sinceStart);

	//==// send data
	byte beginAddress = (entries*5)+2;
	Wire.beginTransmission(eep1);
	Wire.send((int)(beginAddress >> 8));   // MSB
	Wire.send((int)(beginAddress & 0xFF)); // LSB
	Wire.send( (int)((t>>24) & 0x000000FF) );
	Wire.send( (int)((t>>16)&0x000000FF) );
	Wire.send( (int)((t>>8)&0x000000FF) );
	Wire.send( (int)( t &0x000000FF) );
	Wire.send( (user<<2 | (type&0x03)) );
	Wire.endTransmission();
	delay(5);
	//==debug==Serial.println("HERE");
	//==// update num entries
	Wire.beginTransmission(eep1);
	Wire.send((int)(firstpos >> 8));   // MSB
	Wire.send((int)(firstpos & 0xFF)); // LSB
	Wire.send( (((entries + 1)>>8)&0x00FF) );
	Wire.send( ((entries + 1)&0x00FF) );
	Wire.endTransmission();
	delay(5);
	if(verboseon){
		Serial.println("New entry added");
		Serial.print("logEntry took ");
		Serial.print(sinceStart/1000);
		Serial.println(" ms");
	}
}

void setAddress(int deviceAddress, unsigned int address){
	// Send the address to eeprom
	Wire.beginTransmission(deviceAddress);
	Wire.send((int)(address >> 8));   // MSB
	Wire.send((int)(address & 0xFF)); // LSB
	Wire.endTransmission();
}


void dispLogBT(){
	elapsedMicros sinceStart; //testing; time execution
	byte data = 0x00;
	int entries = 0;
	// Get num entries stored 1st posistion //
	data = readEEPROM(eep1,0);
	entries = data<<8; //upper byte
	data = readEEPROM(eep1,1);
	entries = entries | data; //lower byte
	//Serial2.print("Num Entries: ");
	//Serial2.println(entries);
	//Serial2.println("");

	int i = 0;
	Entry a ={(time_t)0,0,0};
	for(i = 0; i<entries; i++){
		a = getEntry(i);
//		Serial3.write('|');
//		Serial3.print((int)Teensy3Clock.get());
//		Serial3.write('~');

		//Serial2.print("Entry number: ");
		Serial2.print(i);
		//Serial2.print("Time: ");
		Serial2.print(",");
		digitalClockDisplay2(a.time);
		//Serial2.print("Log Type: ");
		Serial2.print(a.entryType,DEC);
		if(a.entryType == 1){
			Serial2.print(",");
			Serial2.println(a.user,DEC);
			Serial2.println("");
		}
		Serial2.write('\n');
	}
	if(verboseon){
		Serial.print("dispLogBT took: ");
		Serial.print( sinceStart/1000 );
		Serial.println(" ms");
	}

}
void dispLogWIFI(){
	elapsedMicros sinceStart; //testing; time execution
	byte data = 0x00;
	int entries = 0;
	// Get num entries stored 1st posistion //
	data = readEEPROM(eep1,0);
	entries = data<<8; //upper byte
	data = readEEPROM(eep1,1);
	entries = entries | data; //lower byte
	//Serial2.print("Num Entries: ");
	//Serial2.println(entries);
	//Serial2.println("");

	int i = 0;
	Entry a ={(time_t)0,0,0};
	for(i = 0; i<entries; i++){
		a = getEntry(i);
//		Serial3.write('|');
//		Serial3.print((int)Teensy3Clock.get());
//		Serial3.write('~');

		//Serial2.print("Entry number: ");
		Serial3.print(i);
		//Serial2.print("Time: ");
		Serial3.print(",");
		digitalClockDisplay3(a.time);
		//Serial2.print("Log Type: ");
		Serial3.print(a.entryType,DEC);
		if(a.entryType == 1){
			Serial3.print(",");
			Serial3.println(a.user,DEC);
			Serial3.println("");
		}
		Serial3.write('\n');
	}
	if(verboseon){
		Serial.print("dispLogWIFI took: ");
		Serial.print( sinceStart/1000 );
		Serial.println(" ms");
	}

}
void dispLog(){
	elapsedMicros sinceStart; //testing; time execution
	byte data = 0x00;
	int entries = 0;
	// Get num entries stored 1st posistion //
	data = readEEPROM(eep1,0);
	entries = data<<8; //upper byte
	data = readEEPROM(eep1,1);
	entries = entries | data; //lower byte
	Serial.print("Num Entries: ");
	Serial.println(entries);
	Serial.println("");
	//Serial.println((int)sinceStart);

	int i = 0;
	Entry a ={(time_t)0,0,0};
	for(i = 0; i<entries; i++){
		a = getEntry(i);
//		Serial3.write('|');
//		Serial3.print((int)Teensy3Clock.get());
//		Serial3.write('~');

		Serial.print("Entry number: ");
		Serial.println(i);
		Serial.print("Time: ");
		digitalClockDisplay(a.time);
		Serial.print("Log Type: ");
		Serial.println(a.entryType,DEC);
		Serial.print("User: ");
		Serial.println(a.user,DEC);
		Serial.println("");
	}
	if(verboseon){
		Serial.print("dispLog took: ");
		Serial.print( sinceStart/1000 );
		Serial.println(" ms");
	}

}

void digitalClockDisplay(time_t t) {
	// digital clock display of the time
	Serial.print(hour(t));
	printDigits(minute(t));
	printDigits(second(t));
	Serial.print(" ");
	Serial.print(month(t));
	Serial.print("/");
	Serial.print(day(t));
	Serial.print("/");
	Serial.print(year(t));
	//	Serial.println();
	Serial.write(10);
}
void printDigits(int digits){
	// utility function for digital clock display: prints preceding colon and leading 0
	Serial.print(":");
	if(digits < 10)
		Serial.print('0');
	Serial.print(digits);
}

void digitalClockDisplay2(time_t t) {
	// digital clock display of the time
	printDigits2(hour(t));
	Serial2.print(":");
	printDigits2(minute(t));
	Serial2.print(":");
	printDigits2(second(t));
	Serial2.print(",");
	printDigits2(month(t));
	Serial2.print("/");
	printDigits2(day(t));
	Serial2.print("/");
	Serial2.print(year(t));
	//	Serial.println();
	Serial2.write(",");
}
void printDigits2(int digits){
	// utility function for digital clock display: prints preceding colon and leading 0
	if(digits < 10)
		Serial2.print('0');
	Serial2.print(digits);
}

void digitalClockDisplay3(time_t t) {
	// digital clock display of the time
	printDigits3(hour(t));
	Serial3.print(":");
	printDigits3(minute(t));
	Serial3.print(":");
	printDigits3(second(t));
	Serial3.print(",");
	printDigits3(month(t));
	Serial3.print("/");
	printDigits3(day(t));
	Serial3.print("/");
	Serial3.print(year(t));
	//	Serial.println();
	Serial3.write(",");
}
void printDigits3(int digits){
	// utility function for digital clock display: prints preceding colon and leading 0
	if(digits < 10)
		Serial3.print('0');
	Serial3.print(digits);
}

Entry getEntry(int entNum){
	unsigned int entryStart = (5*entNum)+2;
	setAddress(eep1,entryStart);
	Wire.requestFrom(eep1,5);
	byte arr[5];
	int index=0;
	while(Wire.available()){
		arr[index]=Wire.receive();
		index++;
	}
	uint32_t byte0 = (uint32_t)arr[0];
	uint32_t byte1 = (uint32_t)arr[1];
	uint32_t byte2 = (uint32_t)arr[2];
	uint32_t byte3 = (uint32_t)arr[3];
	time_t ret_t = (time_t)(byte0<<24 | byte1<<16 | byte2<<8 | byte3);
	Entry a = {ret_t, arr[4]>>2, arr[4]&0x03};
	return a;
}

time_t tmConvert_t(int YYYY, byte MM, byte DD, byte hh, byte mm, byte ss){
	tmElements_t tmSet;
	tmSet.Year = YYYY - 1970;
	tmSet.Month = MM;
	tmSet.Day = DD;
	tmSet.Hour = hh;
	tmSet.Minute = mm;
	tmSet.Second = ss;
	return makeTime(tmSet);         //convert to time_t
}

int extract(char *ret, char *arr, int pos){
	int maxEntryLength = 50; //if a value takes more than a specified length, the array is not changed
	int i = 0, j = 0, k = 0;
	while(pos > i){
		if(arr[j] == ','){
			i++;
		}
		j++;
		if(j>maxEntryLength){
			break;
		}
	}
	//Serial.print("index = ");
	//Serial.println(j);
	while( !(arr[j] == ',' || arr[j] == 0) ){
		if(j>maxEntryLength){
			break;
		}
		ret[k] = arr[j];
		k++;
		j++;
	}
	return k;
}

int extractall(char *ret, char *arr, int pos){
	int maxEntryLength = 50; //if a value takes more than a specified length, the array is not changed
	int i = 0, j = 0, k = 0;
	while(pos > i){
		if(arr[j] == ','){
			i++;
		}
		j++;
		if(j>maxEntryLength){
			break;
		}
	}
	//Serial.print("index = ");
	//Serial.println(j);
	while( arr[j] != 0 ){
		if(j>maxEntryLength){
			break;
		}
		ret[k] = arr[j];
		k++;
		j++;
	}
	return k;
}

void chara2uinta(uint8_t *returnarr, char* inarr){
	int i = 0;
	while(inarr[i] != 0){
		returnarr[i] = (uint8_t)inarr[i];
		i++;
	}
}

void print( char *arr){
	//print until 0
	int i = 0;
	char temp = arr[0];
	while( temp!= 0){
		Serial.print(temp);
		i++;
		temp = arr[i];
	}
	Serial.println("");
}

time_t getTeensy3Time(){
	return Teensy3Clock.get();
}


byte strCmp(char * arr, char * compto){
	int i = 0, j = 0;
	while( !(arr[i] == 0 || compto[i] == 0)  ){
		if (arr[i] != compto [i]){
			return 0;
		}
		i++;
	}
	if (arr[i] != compto [i]){
		return 0;
	}
	else
		return 1;
}

void lockDoor(){
	digitalWrite(16,HIGH);
	delay(55);
	digitalWrite(16,LOW);
}

void unlockDoor(){
	Serial.print("trying to unlock");
	digitalWrite(17,HIGH);
	delay(200);
	digitalWrite(17,LOW);
	delay(100);
	digitalWrite(15,HIGH);
	delay(200);
	digitalWrite(15,LOW);
	delay(100);
	digitalWrite(17,HIGH);
	delay(200);
	digitalWrite(17,LOW);
	delay(100);
	digitalWrite(15,HIGH);
	delay(200);
	digitalWrite(15,LOW);
	delay(100);
}




