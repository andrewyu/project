/*
 * RSSI.h
 *
 *  Created on: Apr 1, 2014
 *      Author: Computer
 */

#ifndef RSSI_H_
#define RSSI_H_

class RSSI {
public:
	RSSI();
	virtual ~RSSI();
	void pushRSSI(int);
	int getRSSI();
	void clearRSSI();
private:
	int _rssi1;
	int _rssi2;
	int _rssi3;
};

#endif /* RSSI_H_ */
