// Do not remove the include below
#include "SerialTest.h"
#include "Entry.h"
#include "EEP.h"
#include <Wire.h>
#include <Time.h>
#define eep1 0x50
#define firstpos 0
#define connected 1
#define disconnected 0
#define verboseon 0

void logEntry(time_t time, byte user, byte type);
void writeEEPROM(int deviceaddress, unsigned int eeaddress, byte data ) ;
void setAddress(int deviceAddress, unsigned int address);
byte readEEPROM(int deviceaddress, unsigned int eeaddress );
void dispLog();
//typedef struct Entry{
//	time_t time;
//	byte user;
//	byte entryType;
//} Entry;
void printDigits(int digits);
void digitalClockDisplay(time_t t);
Entry getEntry(int entNum);
time_t tmConvert_t(int YYYY, byte MM, byte DD, byte hh, byte mm, byte ss);
int extract(char *ret, char *arr, int pos);
void print( char *arr);
time_t getTeensy3Time();
byte strCmp(char * arr, char * compto);

void setup()
{
	//Teensy3Clock.set((time_t)(int)1393604046);
	setSyncProvider(getTeensy3Time);
	Wire.begin();
	Serial.begin(115200);
	Serial2.begin(115200);
	unsigned int address = 0;
	writeEEPROM(eep1, address, 0);
	writeEEPROM(eep1, address+1, 0);
	////Serial.print(readEEPROM(eep1, address), DEC);
	delay(5000);
	logEntry((time_t)0x00000000,0, 0);


	EEP eeprom1(0x50);
	Entry a = eeprom1.readEntry(1);
	Serial.print("Entry number: ");
	Serial.println(0);
	Serial.print("Time: ");
	digitalClockDisplay(a.time);
	Serial.print("Log Type: ");
	Serial.println(a.entryType,DEC);
	Serial.print("User: ");
	Serial.println(a.user,DEC);
	Serial.println("");

}



char testarr[] = "nosettime,14,02,28,02,29,50";
//void synctime(char arr[]){

char receiverarray[50];
char receiver2array[50];
char command[10];
byte BTconStatus = disconnected;

void loop(){
	//dispLog();
	if(Serial.available()){
		Serial.readBytesUntil(10,receiverarray,50);
	}
	if(Serial2.available()){
		Serial2.readBytesUntil(0xD,receiver2array,50);
		print(receiver2array);
	}
	int sizee = extract(command,receiver2array, 0); //use size of command for switch
	if(strCmp(command,"CONNECT")){//make another array for serial2
		BTconStatus = connected;
		memset(command,0,10);
		memset(receiver2array,0,50);
	}
	else if(strCmp(command,"DISCONNECT")){
		BTconStatus = disconnected;
		memset(command,0,10);
		memset(receiver2array,0,50);
	}
	extract(command, receiverarray, 0);
	if(strCmp(command,"settime")){
		char t[2];
		int yr;
		byte MM,DD,hh,mm,ss;
		extract(t,receiverarray,1);
		yr = 2000 + (int)((t[0]-48)*10) + (int)(t[1]-48);
		extract(t,receiverarray,2);
		MM = (int)((t[0]-48)*10) + (int)(t[1]-48);
		extract(t,receiverarray,3);
		DD = (int)((t[0]-48)*10) + (int)(t[1]-48);
		extract(t,receiverarray,4);
		hh = (int)((t[0]-48)*10) + (int)(t[1]-48);
		extract(t,receiverarray,5);
		mm = (int)((t[0]-48)*10) + (int)(t[1]-48);
		extract(t,receiverarray,6);
		ss = (int)((t[0]-48)*10) + (int)(t[1]-48);
		Teensy3Clock.set(tmConvert_t(yr,MM,DD,hh,mm,ss));
		memset(command,0,10);
		memset(receiverarray,0,50);
	}

	if(strCmp(command,"lognow")){
		logEntry(getTeensy3Time(), 1,1);
		memset(command,0,10);
		memset(receiverarray,0,50);
	}

	if(strCmp(command,"displaylog")){
		dispLog();
		memset(command,0,10);
		memset(receiverarray,0,50);
	}

	if(strCmp(command,"time?")){
		digitalClockDisplay(Teensy3Clock.get());
		memset(command,0,10);
		memset(receiverarray,0,50);
	}

	if(strCmp(command,"BTstatus?")){
		if(BTconStatus == connected){
			Serial.println("BT is connected");
		}
		else{
			Serial.println("BT is Disconnected");
		}
		memset(command,0,10);
		memset(receiverarray,0,50);
	}

	//	if(strCmp(command,"BTsend")){
	//		char test[20];
	//		int size = extract(test,receiverarray,1);
	//		Serial2.write(test,size);
	//		Serial2.write(0xD);
	//		memset(command,0,10);
	//		memset(receiverarray,0,50);
	//		}
	memset(command,0,10);
	memset(receiverarray,0,50);
	memset(receiver2array,0,50);
	delay(50);
}

void writeEEPROM(int deviceaddress, unsigned int eeaddress, byte data )
{
	Wire.beginTransmission(deviceaddress);
	Wire.send((int)(eeaddress >> 8));   // MSB
	Wire.send((int)(eeaddress & 0xFF)); // LSB
	Wire.send(data);
	Wire.endTransmission();
	delay(5);
}

byte readEEPROM(int deviceaddress, unsigned int eeaddress )
{
	byte rdata = 0xFF;
	Wire.beginTransmission(deviceaddress);
	Wire.send((int)(eeaddress >> 8));   // MSB
	Wire.send((int)(eeaddress & 0xFF)); // LSB
	Wire.endTransmission();

	Wire.requestFrom(deviceaddress,1);

	if (Wire.available()) rdata = Wire.receive();

	return rdata;
}

// Note on EEPROM
// Only one EEPROM is used, so device address is hard coded
// Data is structured so that address 0 contains # of entries
// each entries uses 4 bytes for time,
// 1 byte for type of entry/user
// 11111100 door open
// 11111101 door close
// 6bit usercode + 10 door unlocked
// 11111111 door locked
void logEntry(time_t t, byte user, byte type){
	// get num entries, calculate writing pos, write 't' and 'user',
	// increment num entries

	elapsedMicros sinceStart; //testing; time execution
	byte data = 0x00;
	int entries = 0;
	// Get num entries stored 1st posistion //
	data = readEEPROM(eep1,0);
	entries = data<<8; //upper byte
	data = readEEPROM(eep1,1);
	entries = entries | data; //lower byte
	if(verboseon){
		Serial.print(entries);
		Serial.println(" entries previously stored.");
	}
	//==debug==Serial.println((int)sinceStart);

	//==// send data
	byte beginAddress = (entries*5)+2;
	Wire.beginTransmission(eep1);
	Wire.send((int)(beginAddress >> 8));   // MSB
	Wire.send((int)(beginAddress & 0xFF)); // LSB
	Wire.send( (int)((t>>24) & 0x000000FF) );
	Wire.send( (int)((t>>16)&0x000000FF) );
	Wire.send( (int)((t>>8)&0x000000FF) );
	Wire.send( (int)( t &0x000000FF) );
	Wire.send( (user<<2 | (type&0x03)) );
	Wire.endTransmission();
	delay(5);
	//==debug==Serial.println("HERE");
	//==// update num entries
	Wire.beginTransmission(eep1);
	Wire.send((int)(firstpos >> 8));   // MSB
	Wire.send((int)(firstpos & 0xFF)); // LSB
	Wire.send( (((entries + 1)>>8)&0x00FF) );
	Wire.send( ((entries + 1)&0x00FF) );
	Wire.endTransmission();
	delay(5);
	if(verboseon){
		Serial.println("New entry added");
		Serial.print("logEntry took ");
		Serial.print(sinceStart/1000);
		Serial.println(" ms");
	}
}

void setAddress(int deviceAddress, unsigned int address){
	// Send the address to eeprom
	Wire.beginTransmission(deviceAddress);
	Wire.send((int)(address >> 8));   // MSB
	Wire.send((int)(address & 0xFF)); // LSB
	Wire.endTransmission();
}


void dispLog(){
	elapsedMicros sinceStart; //testing; time execution
	byte data = 0x00;
	int entries = 0;
	// Get num entries stored 1st posistion //
	data = readEEPROM(eep1,0);
	entries = data<<8; //upper byte
	data = readEEPROM(eep1,1);
	entries = entries | data; //lower byte
	Serial.print("Num Entries: ");
	Serial.println(entries);
	Serial.println("");
	//Serial.println((int)sinceStart);

	int i = 0;
	Entry a ={(time_t)0,0,0};
	for(i = 0; i<entries; i++){
		a = getEntry(i);
		Serial.print("Entry number: ");
		Serial.println(i);
		Serial.print("Time: ");
		digitalClockDisplay(a.time);
		Serial.print("Log Type: ");
		Serial.println(a.entryType,DEC);
		Serial.print("User: ");
		Serial.println(a.user,DEC);
		Serial.println("");
	}
	if(verboseon){
		Serial.print("dispLog took: ");
		Serial.print( sinceStart/1000 );
		Serial.print(" ms");
	}

}


void digitalClockDisplay(time_t t) {
	// digital clock display of the time
	Serial.print(hour(t));
	printDigits(minute(t));
	printDigits(second(t));
	Serial.print(" ");
	Serial.print(month(t));
	Serial.print("/");
	Serial.print(day(t));
	Serial.print("/");
	Serial.print(year(t));
//	Serial.println();
	Serial.write(10);
}
void printDigits(int digits){
	// utility function for digital clock display: prints preceding colon and leading 0
	Serial.print(":");
	if(digits < 10)
		Serial.print('0');
	Serial.print(digits);
}
Entry getEntry(int entNum){
	unsigned int entryStart = (5*entNum)+2;
	setAddress(eep1,entryStart);
	Wire.requestFrom(eep1,5);
	byte arr[5];
	int index=0;
	while(Wire.available()){
		arr[index]=Wire.receive();
		index++;
	}
	uint32_t byte0 = (uint32_t)arr[0];
	uint32_t byte1 = (uint32_t)arr[1];
	uint32_t byte2 = (uint32_t)arr[2];
	uint32_t byte3 = (uint32_t)arr[3];
	time_t ret_t = (time_t)(byte0<<24 | byte1<<16 | byte2<<8 | byte3);
	Entry a = {ret_t, arr[4]>>2, arr[4]&0x03};
	return a;
}

time_t tmConvert_t(int YYYY, byte MM, byte DD, byte hh, byte mm, byte ss){
	tmElements_t tmSet;
	tmSet.Year = YYYY - 1970;
	tmSet.Month = MM;
	tmSet.Day = DD;
	tmSet.Hour = hh;
	tmSet.Minute = mm;
	tmSet.Second = ss;
	return makeTime(tmSet);         //convert to time_t
}

int extract(char *ret, char *arr, int pos){
	int maxEntryLength = 50; //if a value takes more than a specified length, the array is not changed
	int i = 0, j = 0, k = 0;
	while(pos > i){
		if(arr[j] == ','){
			i++;
		}
		j++;
		if(j>maxEntryLength){
			break;
		}
	}
	//Serial.print("index = ");
	//Serial.println(j);
	while( !(arr[j] == ',' || arr[j] == 0) ){
		if(j>maxEntryLength){
			break;
		}
		ret[k] = arr[j];
		k++;
		j++;
	}
	return k;
}

void print( char *arr){
	//print until 0
	int i = 0;
	char temp = arr[0];
	while( temp!= 0){
		Serial.print(temp);
		i++;
		temp = arr[i];
	}
	Serial.println("");
}

time_t getTeensy3Time(){
	return Teensy3Clock.get();
}


byte strCmp(char * arr, char * compto){
	int i = 0, j = 0;
	while( !(compto[i] == 0 || compto[i] == 0)  ){
		if (arr[i] != compto [i]){
			return 0;
		}
		i++;
	}
	if (arr[i] != compto [i]){
		return 0;
	}
	else
		return 1;
}
