// Do not remove the include below
#include "testetetest.h"

char inputArray[20];
char *ptr = &inputArray[0];
uint8_t numbytes;
void print(char * arr[]);
//The setup function is called once at startup of the sketch
void setup()
{
// Add your initialization code here
	Serial.begin(115200);
}

// The loop function is called in an endless loop
void loop()
{
//Add your repeated code here
	if(Serial.available()){
		numbytes = readBytesUntil('A', inputArray, 20);
		print(&inputArray[0]);
	}
}

void print( char * arr[]){
	uint8_t i = 0;
	for( i = 0; i<numbytes; i++){
		Serial.print(arr[i]);
	}
}
