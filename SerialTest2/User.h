/*
 * User.h
 *
 *  Created on: Apr 2, 2014
 *      Author: Computer
 */

#ifndef USER_H_
#define USER_H_

#include "SerialTest2.h"

class User {
public:
	User();
	User(String,int);
	User(String,int,String);
	User(String,int,time_t,time_t);
	User(String,int,time_t,time_t,String);
	virtual ~User();

	int isAllowed();
	void setAlias(String);
	void changeEndTime(time_t);
	void changeStartTime(time_t);
	String getUserId();
	int getLogId();
	void setLogId(int);
	void setUserID(String);
	String getAlias();
	time_t getStartTime();
	time_t getEndTime();
	void disallow();
private:
	time_t _allowedBegin;
	time_t _allowedEnd;
	int _logId;
	String _userId;
	String _alias;
};

#endif /* USER_H_ */
