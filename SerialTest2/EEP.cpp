/*
 * EEP.cpp
 *
 *  Created on: Feb 28, 2014
 *      Author: Computer
 */

#include "EEP.h"
#include "Entry.h"
#include <Wire.h>

EEP::EEP(uint8_t address)
:_eeAddress(address)
{
	// TODO Auto-generated constructor stub
	//eeAddress = address;
}

void EEP::setAddress(unsigned int address){
	Wire.beginTransmission(_eeAddress);
	Wire.send((int)(address >> 8));   // MSB
	Wire.send((int)(address & 0xFF)); // LSB
	Wire.endTransmission();
}

void EEP::writeByte(unsigned int eePosition, byte data){
	Wire.beginTransmission(_eeAddress);
	Wire.send((int)(eePosition >> 8));   // MSB
	Wire.send((int)(eePosition & 0xFF)); // LSB
	Wire.send(data);
	Wire.endTransmission();
	delay(5);
}

byte EEP::readByte(unsigned int eePosition){

	setAddress(eePosition);
	Wire.requestFrom((int)_eeAddress,1);

//	ADD ERROR CHECKING, TIMEOUT WHEN NO DATA RECEIVED
//	elapsedMillis sinceRequestStart;
//	while( !(Wire.available()) ){
//		if(sinceRequestStart>100){
//			return 0xFF;
//		}
//	}
	byte temp;
	if(Wire.available()){
		temp = Wire.receive();
	}
	return temp;
}

Entry EEP::getEntry(int pos){
	unsigned int entryStart = (5*pos)+2;
	setAddress(entryStart);
	Wire.requestFrom((int)_eeAddress,5);
	byte arr[5];
	int index=0;
	while(Wire.available()){
		arr[index]=Wire.receive();
		index++;
	}
	uint32_t byte0 = (uint32_t)arr[0];
	uint32_t byte1 = (uint32_t)arr[1];
	uint32_t byte2 = (uint32_t)arr[2];
	uint32_t byte3 = (uint32_t)arr[3];
	time_t ret_t = (time_t)(byte0<<24 | byte1<<16 | byte2<<8 | byte3);
	Entry a = {ret_t, arr[4]>>2, arr[4]&0x03};
	return a;
}

int EEP::getNumEntries(){
	byte temp;
	int numEntries=0;

	temp = readByte(0);
	numEntries = temp<<8; //MSB of 16bit number
	temp = readByte(1);
	numEntries |= temp;   //LSB of 16bit number

	return numEntries;
}

int EEP::logEntry(time_t time,byte user,byte type){
	elapsedMillis sinceStart;
	int prevEntries = getNumEntries();
	int beginAddress = (prevEntries*5)+2;
	Wire.beginTransmission(_eeAddress);
	Wire.send((beginAddress >> 8));   // MSB
	Wire.send((beginAddress & 0xFF)); // LSB
	Wire.send( (int)((time>>24) & 0x000000FF) );
	Wire.send( (int)((time>>16)&0x000000FF) );
	Wire.send( (int)((time>>8)&0x000000FF) );
	Wire.send( (int)( time & 0x000000FF) );
	Wire.send( (user<<2 | (type&0x03)) );
	Wire.endTransmission();
	delay(5);

	writeByte(0,(prevEntries+1)>>8);
	writeByte(1,((prevEntries+1) & 0x00FF) );
}

void EEP::clearLog(){
	writeByte(0,0);
	writeByte(1,0);
}

EEP::~EEP() {
	// TODO Auto-generated destructor stub
}

