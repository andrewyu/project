/*
 * EEP.h
 *
 *  Created on: Feb 28, 2014
 *      Author: Computer
 */

#ifndef EEP_H_
#define EEP_H_

#include <arduino.h>
#include "Entry.h"

class EEP {
public:
//	typedef struct Entry{
//		time_t time;
//		byte user;
//		byte entryType;
//	} Entry;
	EEP(uint8_t);
	EEP();
	void setDeviceAddress(uint8_t);
	virtual ~EEP();
	void setAddress(unsigned int);
	void writeByte(unsigned int, byte);
	byte readByte(unsigned int);
	Entry getEntry(int);
	int getNumEntries();
private:
	uint8_t _eeAddress;
};

#endif /* EEP_H_ */
