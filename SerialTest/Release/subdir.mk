################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../EEP.cpp \
../SerialTest.cpp 

CPP_DEPS += \
./EEP.cpp.d \
./SerialTest.cpp.d 

LINK_OBJ += \
./EEP.cpp.o \
./SerialTest.cpp.o 


# Each subdirectory must supply rules for building sources it contributes
EEP.cpp.o: ../EEP.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-g++" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib -fno-rtti -fno-exceptions -mcpu=cortex-m4 -DF_CPU=96000000 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__  -felide-constructors -std=gnu++0x   -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

SerialTest.cpp.o: ../SerialTest.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-g++" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib -fno-rtti -fno-exceptions -mcpu=cortex-m4 -DF_CPU=96000000 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__  -felide-constructors -std=gnu++0x   -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '


