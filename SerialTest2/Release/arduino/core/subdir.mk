################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/HardwareSerial1.cpp \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/HardwareSerial2.cpp \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/HardwareSerial3.cpp \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/IPAddress.cpp \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/IntervalTimer.cpp \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/Print.cpp \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/Stream.cpp \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/Tone.cpp \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/WMath.cpp \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/WString.cpp \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/main.cpp \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/usb_flightsim.cpp \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/usb_inst.cpp 

C_SRCS += \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/analog.c \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/eeprom.c \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/keylayouts.c \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/math_helper.c \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/mk20dx128.c \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/nonstd.c \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/pins_teensy.c \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/serial1.c \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/serial2.c \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/serial3.c \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/touch.c \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/usb_desc.c \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/usb_dev.c \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/usb_joystick.c \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/usb_keyboard.c \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/usb_mem.c \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/usb_midi.c \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/usb_mouse.c \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/usb_rawhid.c \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/usb_seremu.c \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/usb_serial.c \
C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/yield.c 

C_DEPS += \
./arduino/analog.c.d \
./arduino/eeprom.c.d \
./arduino/keylayouts.c.d \
./arduino/math_helper.c.d \
./arduino/mk20dx128.c.d \
./arduino/nonstd.c.d \
./arduino/pins_teensy.c.d \
./arduino/serial1.c.d \
./arduino/serial2.c.d \
./arduino/serial3.c.d \
./arduino/touch.c.d \
./arduino/usb_desc.c.d \
./arduino/usb_dev.c.d \
./arduino/usb_joystick.c.d \
./arduino/usb_keyboard.c.d \
./arduino/usb_mem.c.d \
./arduino/usb_midi.c.d \
./arduino/usb_mouse.c.d \
./arduino/usb_rawhid.c.d \
./arduino/usb_seremu.c.d \
./arduino/usb_serial.c.d \
./arduino/yield.c.d 

CPP_DEPS += \
./arduino/HardwareSerial1.cpp.d \
./arduino/HardwareSerial2.cpp.d \
./arduino/HardwareSerial3.cpp.d \
./arduino/IPAddress.cpp.d \
./arduino/IntervalTimer.cpp.d \
./arduino/Print.cpp.d \
./arduino/Stream.cpp.d \
./arduino/Tone.cpp.d \
./arduino/WMath.cpp.d \
./arduino/WString.cpp.d \
./arduino/main.cpp.d \
./arduino/usb_flightsim.cpp.d \
./arduino/usb_inst.cpp.d 

LINK_OBJ += \
./arduino/HardwareSerial1.cpp.o \
./arduino/HardwareSerial2.cpp.o \
./arduino/HardwareSerial3.cpp.o \
./arduino/IPAddress.cpp.o \
./arduino/IntervalTimer.cpp.o \
./arduino/Print.cpp.o \
./arduino/Stream.cpp.o \
./arduino/Tone.cpp.o \
./arduino/WMath.cpp.o \
./arduino/WString.cpp.o \
./arduino/analog.c.o \
./arduino/eeprom.c.o \
./arduino/keylayouts.c.o \
./arduino/main.cpp.o \
./arduino/math_helper.c.o \
./arduino/mk20dx128.c.o \
./arduino/nonstd.c.o \
./arduino/pins_teensy.c.o \
./arduino/serial1.c.o \
./arduino/serial2.c.o \
./arduino/serial3.c.o \
./arduino/touch.c.o \
./arduino/usb_desc.c.o \
./arduino/usb_dev.c.o \
./arduino/usb_flightsim.cpp.o \
./arduino/usb_inst.cpp.o \
./arduino/usb_joystick.c.o \
./arduino/usb_keyboard.c.o \
./arduino/usb_mem.c.o \
./arduino/usb_midi.c.o \
./arduino/usb_mouse.c.o \
./arduino/usb_rawhid.c.o \
./arduino/usb_seremu.c.o \
./arduino/usb_serial.c.o \
./arduino/yield.c.o 


# Each subdirectory must supply rules for building sources it contributes
arduino/HardwareSerial1.cpp.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/HardwareSerial1.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-g++" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib -fno-rtti -fno-exceptions -mcpu=cortex-m4 -DF_CPU=48000000 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__  -felide-constructors -std=gnu++0x   -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/HardwareSerial2.cpp.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/HardwareSerial2.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-g++" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib -fno-rtti -fno-exceptions -mcpu=cortex-m4 -DF_CPU=48000000 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__  -felide-constructors -std=gnu++0x   -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/HardwareSerial3.cpp.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/HardwareSerial3.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-g++" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib -fno-rtti -fno-exceptions -mcpu=cortex-m4 -DF_CPU=48000000 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__  -felide-constructors -std=gnu++0x   -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/IPAddress.cpp.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/IPAddress.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-g++" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib -fno-rtti -fno-exceptions -mcpu=cortex-m4 -DF_CPU=48000000 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__  -felide-constructors -std=gnu++0x   -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/IntervalTimer.cpp.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/IntervalTimer.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-g++" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib -fno-rtti -fno-exceptions -mcpu=cortex-m4 -DF_CPU=48000000 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__  -felide-constructors -std=gnu++0x   -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/Print.cpp.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/Print.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-g++" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib -fno-rtti -fno-exceptions -mcpu=cortex-m4 -DF_CPU=48000000 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__  -felide-constructors -std=gnu++0x   -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/Stream.cpp.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/Stream.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-g++" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib -fno-rtti -fno-exceptions -mcpu=cortex-m4 -DF_CPU=48000000 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__  -felide-constructors -std=gnu++0x   -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/Tone.cpp.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/Tone.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-g++" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib -fno-rtti -fno-exceptions -mcpu=cortex-m4 -DF_CPU=48000000 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__  -felide-constructors -std=gnu++0x   -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/WMath.cpp.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/WMath.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-g++" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib -fno-rtti -fno-exceptions -mcpu=cortex-m4 -DF_CPU=48000000 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__  -felide-constructors -std=gnu++0x   -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/WString.cpp.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/WString.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-g++" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib -fno-rtti -fno-exceptions -mcpu=cortex-m4 -DF_CPU=48000000 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__  -felide-constructors -std=gnu++0x   -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/analog.c.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/analog.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-gcc" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib  -mcpu=cortex-m4 -DF_CPU=48000000 -DTIME_T=1365449461 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__    -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/eeprom.c.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/eeprom.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-gcc" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib  -mcpu=cortex-m4 -DF_CPU=48000000 -DTIME_T=1365449461 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__    -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/keylayouts.c.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/keylayouts.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-gcc" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib  -mcpu=cortex-m4 -DF_CPU=48000000 -DTIME_T=1365449461 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__    -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/main.cpp.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/main.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-g++" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib -fno-rtti -fno-exceptions -mcpu=cortex-m4 -DF_CPU=48000000 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__  -felide-constructors -std=gnu++0x   -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/math_helper.c.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/math_helper.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-gcc" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib  -mcpu=cortex-m4 -DF_CPU=48000000 -DTIME_T=1365449461 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__    -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/mk20dx128.c.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/mk20dx128.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-gcc" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib  -mcpu=cortex-m4 -DF_CPU=48000000 -DTIME_T=1365449461 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__    -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/nonstd.c.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/nonstd.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-gcc" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib  -mcpu=cortex-m4 -DF_CPU=48000000 -DTIME_T=1365449461 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__    -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/pins_teensy.c.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/pins_teensy.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-gcc" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib  -mcpu=cortex-m4 -DF_CPU=48000000 -DTIME_T=1365449461 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__    -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/serial1.c.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/serial1.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-gcc" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib  -mcpu=cortex-m4 -DF_CPU=48000000 -DTIME_T=1365449461 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__    -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/serial2.c.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/serial2.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-gcc" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib  -mcpu=cortex-m4 -DF_CPU=48000000 -DTIME_T=1365449461 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__    -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/serial3.c.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/serial3.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-gcc" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib  -mcpu=cortex-m4 -DF_CPU=48000000 -DTIME_T=1365449461 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__    -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/touch.c.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/touch.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-gcc" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib  -mcpu=cortex-m4 -DF_CPU=48000000 -DTIME_T=1365449461 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__    -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/usb_desc.c.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/usb_desc.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-gcc" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib  -mcpu=cortex-m4 -DF_CPU=48000000 -DTIME_T=1365449461 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__    -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/usb_dev.c.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/usb_dev.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-gcc" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib  -mcpu=cortex-m4 -DF_CPU=48000000 -DTIME_T=1365449461 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__    -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/usb_flightsim.cpp.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/usb_flightsim.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-g++" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib -fno-rtti -fno-exceptions -mcpu=cortex-m4 -DF_CPU=48000000 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__  -felide-constructors -std=gnu++0x   -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/usb_inst.cpp.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/usb_inst.cpp
	@echo 'Building file: $<'
	@echo 'Starting C++ compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-g++" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib -fno-rtti -fno-exceptions -mcpu=cortex-m4 -DF_CPU=48000000 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__  -felide-constructors -std=gnu++0x   -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -x c++ "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/usb_joystick.c.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/usb_joystick.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-gcc" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib  -mcpu=cortex-m4 -DF_CPU=48000000 -DTIME_T=1365449461 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__    -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/usb_keyboard.c.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/usb_keyboard.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-gcc" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib  -mcpu=cortex-m4 -DF_CPU=48000000 -DTIME_T=1365449461 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__    -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/usb_mem.c.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/usb_mem.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-gcc" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib  -mcpu=cortex-m4 -DF_CPU=48000000 -DTIME_T=1365449461 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__    -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/usb_midi.c.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/usb_midi.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-gcc" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib  -mcpu=cortex-m4 -DF_CPU=48000000 -DTIME_T=1365449461 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__    -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/usb_mouse.c.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/usb_mouse.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-gcc" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib  -mcpu=cortex-m4 -DF_CPU=48000000 -DTIME_T=1365449461 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__    -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/usb_rawhid.c.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/usb_rawhid.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-gcc" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib  -mcpu=cortex-m4 -DF_CPU=48000000 -DTIME_T=1365449461 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__    -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/usb_seremu.c.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/usb_seremu.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-gcc" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib  -mcpu=cortex-m4 -DF_CPU=48000000 -DTIME_T=1365449461 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__    -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/usb_serial.c.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/usb_serial.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-gcc" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib  -mcpu=cortex-m4 -DF_CPU=48000000 -DTIME_T=1365449461 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__    -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '

arduino/yield.c.o: C:/arduino-1.5.2/hardware/teensy/all/cores/teensy3/yield.c
	@echo 'Building file: $<'
	@echo 'Starting C compile'
	"C:/arduino-1.5.2/hardware/teensy/tools/windows/arm-none-eabi/bin/arm-none-eabi-gcc" -c -g -Os -w -ffunction-sections -fdata-sections -nostdlib  -mcpu=cortex-m4 -DF_CPU=48000000 -DTIME_T=1365449461 -DUSB_SERIAL -DLAYOUT_US_ENGLISH -MMD -DARDUINO=152  -mthumb  -D__MK20DX128__    -I"C:\arduino-1.5.2\hardware\teensy\all\cores\teensy3" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Wire\utility" -I"C:\arduino-1.5.2\hardware\teensy\all\libraries\Time" -I"C:\arduino-1.5.2\libraries\TimeAlarms" -I"C:\arduino-1.5.2\libraries\EEPROM" -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" "$<"  -o  "$@"
	@echo 'Finished building: $<'
	@echo ' '


