/*
 * User.cpp
 *
 *  Created on: Apr 2, 2014
 *      Author: Computer
 */

#include "User.h"

User::User()
:_userId(""), _logId(-1), _allowedBegin((time_t)0),_allowedEnd((time_t)0),_alias("")
{
}
User::User(String userid, int logid)
: _userId(userid), _logId(logid), _allowedBegin((time_t)0),_allowedEnd((time_t)4102444800),_alias("NOALIAS")
{
}

User::User(String userid, int logid, String alias)
: _userId(userid), _logId(logid), _allowedBegin((time_t)0),_allowedEnd((time_t)4102444800),_alias(alias)
{
}

User::User(String userid, int logid, time_t begin, time_t end)
:_userId(userid), _logId(logid), _allowedBegin(begin),_allowedEnd(end),_alias("NOALIAS")
{
}

User::User(String userid, int logid, time_t begin, time_t end, String alias)
:_userId(userid), _logId(logid), _allowedBegin(begin),_allowedEnd(end),_alias(alias)
{
}
User::~User() {
}

int User::isAllowed(){
	time_t currTime = Teensy3Clock.get();
	if( (currTime > _allowedBegin) && (currTime < _allowedEnd) ){
		return 1;
	}
	else{
		return 0;
	}
}

void User::setAlias(String alias){
	_alias = alias;
}

void User::changeEndTime(time_t end){
	_allowedEnd = end;
}

void User::changeStartTime(time_t begin){
	_allowedBegin = begin;
}

String User::getUserId(){
	return _userId;
}
int User::getLogId(){
	return _logId;
}
String User::getAlias(){
	return _alias;
}
time_t User::getStartTime(){
	return _allowedBegin;
}
time_t User::getEndTime(){
	return _allowedEnd;
}

void User::setLogId(int newlogid){
	_logId = newlogid;
}

void User::setUserID(String newUser){
	_userId = newUser;
}
void User::disallow(){
	_allowedBegin = (time_t)0;
	_allowedEnd = (time_t)0;
}
