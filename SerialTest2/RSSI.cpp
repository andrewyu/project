/*
 * RSSI.cpp
 *
 *  Created on: Apr 1, 2014
 *      Author: Computer
 */

#include "RSSI.h"
#include "Arduino.h"
//#include <math.h>

RSSI::RSSI()
:_rssi1(1000), _rssi2(1000),_rssi3(1000)
{
}

void RSSI::pushRSSI(int newRSSI){
	_rssi3=_rssi2;
	_rssi2=_rssi1;
	_rssi1=newRSSI;
}

int RSSI::getRSSI(){
	int minRSSI = min(_rssi3,_rssi2);
	minRSSI = min(minRSSI,_rssi1);
	return minRSSI;
}

void RSSI::clearRSSI(){
	_rssi1=1000;
	_rssi2=1000;
	_rssi3=1000;
}

RSSI::~RSSI() {
	// TODO Auto-generated destructor stub
}

