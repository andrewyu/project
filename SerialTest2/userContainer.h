/*
 * userContainer.h
 *
 *  Created on: Apr 2, 2014
 *      Author: Computer
 */

#ifndef USERCONTAINER_H_
#define USERCONTAINER_H_
#include "SerialTest2.h"

class userContainer {
public:
	userContainer();
	void addUser(User);
	int getNumUsers();
	int getUserPos(String);
	int getLogId(int);
	String getUserId(int);
	String getAlias(int);
	int isAllowed(int);
	void disAllowUser(int);
	void printUserInfo(int);
	void printUserInfoToWifi(int);
	void printUserInfoAll();
	void printUserInfoAllWifi();
	void clearDisallowed();
	void editUser(int , time_t , time_t , String);
	void allowBTuser(int);
	void backupToEEPROM();
	void loadFromEEPROM();
	virtual ~userContainer();
private:
	int _numUsers;
	User _arrayofUsers[64];
};

#endif /* USERCONTAINER_H_ */
