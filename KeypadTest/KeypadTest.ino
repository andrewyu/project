
//14, 15, 16, 17, 18

const unsigned char numAllowedPasswords = 20;
const unsigned char sizeOfPassword = 5;

unsigned char readRow1();
unsigned char readRow2();
unsigned char getKeyPress();
void getSequence(char length, unsigned char arr[]);
void printuca(unsigned char input[]);
//boolean comparepw(unsigned char a[][], unsigned char b[]);
//boolean checkSeq(unsigned char arr[]);
boolean compare(unsigned char a[], unsigned char b[]);


unsigned char acceptedcodes[][5]={
  {'a','a','b','d','e'},
  {'b','d','d','a','c'}
};//passwordbank
void extractLine( unsigned char *outputArr[], unsigned char inputArr[][5], char row){
  char i = 0;
  for( i = 0; i<5; i++){
    *outputArr[i] = inputArr[row][i];
  }
}



unsigned char acceptedcode[5] = {'a', 'e', 'd', 'c', 'b'};

//=============================================================
void setup(){
  pinMode(14,OUTPUT);
  pinMode(18,OUTPUT);
  
  pinMode(15,INPUT);
  pinMode(16,INPUT);
  pinMode(17,INPUT);

  pinMode(11,OUTPUT);
  pinMode(12,OUTPUT);


  Serial.begin(115200);
  digitalWrite(12,HIGH);
}


void loop(){//================================loop=============
  unsigned char seqLen = 5;
  unsigned char array[sizeOfPassword];
  
  if(readRow2()== 'f'){
    getSequence(seqLen,array);
    printuca(array);
    Serial.print('\n');

    if(compare(acceptedcode,array)){
      digitalWrite(11, HIGH);
      digitalWrite(12, LOW);
    }
    else{
      digitalWrite(11,LOW);
      digitalWrite(12,HIGH);
    }

  }
  delay(100);
}
//=============================================================

unsigned char readRow1(){
  digitalWrite(14,HIGH);
  digitalWrite(18,LOW);
  if(digitalRead(15)==1){
    return 'd';
  }
  else if(digitalRead(16)==1){
    return 'c';
  }
  else if(digitalRead(17)==1){
    return 'b';
  }
  else{
    return 0;
  }
}

unsigned char readRow2(){
  digitalWrite(18,HIGH);
  digitalWrite(14,LOW);
  if(digitalRead(15)==1){
    return 'a';
  }
  else if(digitalRead(16)==1){
    return 'e';
  }
  else if(digitalRead(17)==1){
    return 'f';
  }
  else{
    return 0;
  }
}

unsigned char getKeyPress(){
  unsigned char temp1 = readRow1();
  unsigned char temp2 = readRow2();
  if(temp1==temp2){//both 0
    return 0;
  }
  else if(temp2 == 0){
    return temp1;
  }
  else{
    return temp2;
  }
}

void getSequence(char length, unsigned char arr[]){
  unsigned char i;
  unsigned char pressed = getKeyPress();
  unsigned char buffer = pressed;
  for(i=0;i<length;){//no auto increment
    delay(10);
    pressed = getKeyPress();
    if(pressed != buffer){ // execute if button state changes
      buffer = pressed;
      if( pressed != 0){ //store if not 0
        arr[i] = buffer;//or pressed
        i++;
      }
    }
  }
}


void printuca(unsigned char input[]){
  unsigned char i;
  for(i=0;i<=sizeof(input);i++){
    Serial.write(input[i]);
  }
}

boolean compare(unsigned char a[], unsigned char b[]){
  unsigned char j=0;
  if(sizeof(a) != sizeof(b)){
    return false;
  }
  for(j = 0; j<sizeof(a); j++){
    if(!(a[j] == b[j])){
      return false;
    }
  }
  return true;
}
