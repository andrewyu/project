#include "SerialTest2.h"

//Defines======================================================================
//#define eep1 0x50
//#define firstpos 0
//#define connected 1
//#define disconnected 0
//#define serial 1
//#define bluetooth 2
//#define wifi 3
//#define open 0
//#define closed 1
//#define commandMode 1
//#define dataMode 0
//#define statusupdate_c 0
//#define btsend_c 1
//#define time_c 2
//#define displaylog_c 3
//#define newuser_c 4
//#define settime_c 5
#define verboseon 1
#define rssiUnlockThresh 60
#define rssiLockThresh 80
#define dontLog 1

//Global Variables=============================================================
enum commandenum{statusupdate_c, btsend_c, time_c, displaylog_c, newuser_c, settime_c};
enum BTmode{commandMode, dataMode};
enum BTstatus{disconnected, connected};
enum sender{placeholder, serial, bluetooth, wifi, keypad};
enum doorstatus{closed, open};
enum btcommand{at, atrssi, cmode, dmode, atdh, atdc,atple};
enum lockStatus{locked,unlocked};

EEP entryLog(0x50);
RSSI distance;
int commandSentBy;
int lastCommand;
char receiverarray[150];
char command[20];
int BTconStatus;
String receiverString = "";
String command_s = "";
int BTMode;
boolean isPaired;
boolean pairNext;
boolean BTallowUnlock;
elapsedMillis sinceRSSI;
elapsedMillis sinceConnect;
elapsedMillis sinceLastLockEvent;
elapsedMillis sinceLastAutoLock;
elapsedMillis sinceLastKeyPress;
char lastKeyPress, currentKeyPress;
boolean connectionMode;
volatile boolean doorStatus = closed;
int lockStatus = locked;
int autolockdelay = 30;

char prevKey = 0;
int maxKeypadCodeSize = 10;
int keysEntered = 0;
char keyPressArray[11] = {0};

User user1("SERIAL",0,(time_t)0, (time_t)4102444800, "SerialAdmin");
User user2("WIFI",1,(time_t)0, (time_t)4102444800, "WifiAdmin");
User user3("ABCDE",2,(time_t)0,Teensy3Clock.get()+100000,"KeypadHardCode1");
User user4("BTnoAPP",3,(time_t)0,Teensy3Clock.get()+100000,"BTdevicewithoutAPP");
//User user5("toerase",4,0,0,"toerase");

userContainer container;

String currBTuser = "";
boolean autoLockEnable = true;
//Functions====================================================================
//void logEntry(time_t time, byte user, byte type);
//void writeEEPROM(int deviceaddress, unsigned int eeaddress, byte data ) ;
//void setAddress(int deviceAddress, unsigned int address);
//byte readEEPROM(int deviceaddress, unsigned int eeaddress );
//void dispLogBT();
//void dispLogWIFI();
//void printDigits2(int digits);
//void digitalClockDisplay2(time_t t);
//void printDigits3(int digits);
//void digitalClockDisplay3(time_t t);
//Entry getEntry(int entNum);
void dispLog();
void printDigits(int digits);
void digitalClockDisplay(time_t t);
time_t tmConvert_t(int YYYY, byte MM, byte DD, byte hh, byte mm, byte ss);
int extract(char *ret, char *arr, int pos);
void print( char *arr);
time_t getTeensy3Time();
byte strCmp(char * arr, char * compto);
void chara2uinta(uint8_t *returnarr, char* inarr);
int extractall(char *ret, char *arr, int pos);
void BTcommand(int);
void lockDoor();
void lockDoorNoLog();
void unlockDoor(int logId);
//String removeExtraSpace(String input);
void getInput();
void clearArrays();
void printSender();
void ledLow(){
	//Serial.println("Low Now");
	digitalWrite(13,LOW);
}
void toggleDoorStatus();
void updateConnectionMode();
void processCommand();
String getCommandString(String input);
char getKey();
void redOn();
void greenOn();
void redOff();
void greenOff();
void orangeOn();
void orangeOff();
void led56Low();
void checkAllowUnlock(String entered);
void bufferedGetKey();
void autolock();

// SETUP=======================================================================
void setup()
{
	setSyncProvider(getTeensy3Time); // Set program clock to sync with RTC
	Wire.begin(); // needed for EEPROM
	Serial.begin(115200); //Serial Communicaton
	Serial2.begin(115200); //Bluetooth Communication
	Serial3.begin(19200); //WIFI Communication
	pinMode(12,INPUT_PULLUP); //Reed switch for door open/close
	pinMode(11,INPUT); //Switching RSSI grabbing on/off

	pinMode(23,OUTPUT); // Keypad Red LED
	pinMode(22,OUTPUT); // Keypad Green LED
	pinMode(13,OUTPUT); // on board LED

	//relay pins
	pinMode(15,OUTPUT); // 'E' 9/0
	pinMode(16,OUTPUT); // 'F' lock
	pinMode(17,OUTPUT); // 'A' 1/2

	//keypadpins
	pinMode(0,OUTPUT); // Row 1 (top B C D)
	pinMode(1,OUTPUT); // Row 2  (bottom A F E)
	pinMode(2,INPUT_PULLUP); // F B
	pinMode(3,INPUT_PULLUP); // C E
	pinMode(4,INPUT_PULLUP); // A D

	//container.addUser(user5); //testing deleting users
//		container.addUser(user1); // add hardcoded usrs to list.
//		container.addUser(user2); // does not store to eeprom yet
//		container.addUser(user3);
	//	container.addUser(user4);
	//	container.backupToEEPROM();

	//	container.loadFromEEPROM();

	pinMode(5,OUTPUT); // Lock door signal light
	pinMode(6,OUTPUT); // Unlock door signal light

	delay(5000); //pause to allow serial connection before starting

	//disconnect any connected bluetooth module
	Serial.println("Program Start");
	BTcommand(cmode);
	Serial.println("Disconnecting..");
	getInput();
	processCommand();
	BTcommand(atdh); //May print error if no module connected
	getInput();
	processCommand();
	Serial.print("Number of entries in log: ");
	Serial.println(entryLog.getNumEntries());
	lockDoorNoLog();
	//Alarm.timerRepeat(30,lockDoorNoLog);

	//	String testing = "this is a string";
	//	int lengthofstring = testing.length();
	//	Serial.print(testing.charAt(lengthofstring-1));

	//	int tempi;
	//	for(tempi=0;tempi<64; tempi++){
	//		Serial.println(EEPROM.read(tempi),DEC);
	//	}

	container.loadFromEEPROM();
}

void loop(){
	Alarm.delay(0); //needed for timed events
	autolock();
	if(sinceLastLockEvent>1000 && (digitalRead(5)||digitalRead(6)) ){
		led56Low();
	}
	updateConnectionMode();

	if(digitalRead(12) != doorStatus){
		toggleDoorStatus();
	}

	bufferedGetKey();

	//================
	getInput();
	if(verboseon){
		printSender();
	}
	processCommand();
	//=================
	if(pairNext && BTconStatus==connected){
		pairNext = 0;
		if(!isPaired){
			if(BTMode != commandMode){
				delay(50);
				BTcommand(cmode);
				delay(50);
				getInput();
				processCommand();
			}
			BTcommand(atple);
			getInput();
			processCommand();
			delay(5000);
		}
	}
	String btst = "-";
	String temporaryUserId = btst.append(currBTuser);
	if(BTconStatus==connected && isPaired && (container.getUserPos(currBTuser)==-1) && (container.getUserPos(temporaryUserId)==-1) ){
		User temporaryUser(temporaryUserId,container.getNumUsers(),0,0,"Unidentified BT Device");
		container.addUser(temporaryUser);
		Serial.print("This Paired device is not confirmed and not in the user list:");
		Serial.println(temporaryUser.getUserId());
		Serial.println("Adding new User..");
	}
	//	String btst = "-";
	//	Serial.println(container.getUserPos(btst.append("BTnoAPP")));
	//	Serial.println(container.getUserPos("-BTnoAPP"));

	if(sinceConnect > 5000){
		if(BTconStatus==connected){
			BTcommand(cmode);
			delay(50);
			getInput();
			processCommand();
			BTcommand(atdh);
			delay(50);
			getInput();
			processCommand();
		}
	}
	if(sinceConnect>1000 && connectionMode){
		if(BTconStatus==connected && isPaired && BTMode != commandMode){
			delay(50);
			BTcommand(cmode);
			delay(50);
			getInput();
			processCommand();
		}
		if(BTconStatus==connected && !isPaired){
			if(BTMode != commandMode){
				delay(50);
				BTcommand(cmode);
				delay(50);
				getInput();
				processCommand();
			}
			BTcommand(atdh);
			getInput();
			processCommand();
		}
		if(BTconStatus==connected && sinceRSSI>700 && BTMode==commandMode &&isPaired){
			sinceRSSI = 0;
			BTcommand(atrssi);
			getInput();
			processCommand();
		}
	}

	if(BTconStatus==connected && isPaired && connectionMode){
		if(distance.getRSSI()< rssiUnlockThresh){
			BTallowUnlock = true;
			//Serial.println(distance.getRSSI());
			//Serial.println("CAN UNLOCK =============");
		}
		else{
			BTallowUnlock = false;
		}
		if(distance.getRSSI() > rssiLockThresh){
			lockDoorNoLog();
		}
	}
	if(lockStatus == locked && BTallowUnlock && getKey()=='F' && prevKey!='F'){ //TODO:also check time
		//commandSentBy = bluetooth;
		//unlockDoor();
		Serial.print("currBTuser: ");
		Serial.println(currBTuser);
		//unlockDoor(4);
		checkAllowUnlock(currBTuser);
	}
}

void dispLog(){
	elapsedMicros sinceStart; //testing; time execution

	int entries = entryLog.getNumEntries();
	int i = 0;
	Entry a;
	switch(commandSentBy){
	case serial:
		for(i = 0; i<entries; i++){
			a = entryLog.getEntry(i);
			//Serial.print("Entry number: ");
			Serial.print(i+1);
			Serial.print(',');
			digitalClockDisplay(a.time);
			Serial.print(',');
			Serial.print(a.user,DEC);
			Serial.print(',');
			Serial.print(a.entryType,DEC);
			Serial.print('\n');
		}
		break;

	case bluetooth: //TODO: make into string before sending
		for(i = 0; i<entries; i++){

			a = entryLog.getEntry(i);
			int j = i+1;
			String temp;
			temp = String(temp + j);
			temp = String(temp + ',');
			temp = String(temp + (int)hour(a.time) );
			temp = String(temp + ':');
			temp = String(temp + (int)minute(a.time) );
			temp = String(temp + ':');
			temp = String(temp + (int)second(a.time) );
			temp = String(temp + ',');
			temp = String(temp + (int)month(a.time) );
			temp = String(temp + '/');
			temp = String(temp + (int)day(a.time) );
			temp = String(temp + '/');
			temp = String(temp + (int)year(a.time) );
			temp = String(temp + ',');
			temp = String(temp + (int)a.user);
			temp = String(temp + ',');
			temp = String(temp + (int)a.entryType);

			Serial2.print(temp);
			Serial2.write(0xd);
			delay(50);
			if((i%10)==0){
				delay(50);
			}

			//			a = entryLog.getEntry(i);
			//			Serial2.print(i+1);
			//			Serial2.print(',');
			//			digitalClockDisplay(a.time);
			//			Serial2.print(',');
			//			Serial2.print(a.user,DEC);
			//			Serial2.print(',');
			//			Serial2.print(a.entryType,DEC);
			//			Serial2.write(0xd);
		}
		break;

	case wifi:
		Serial3.print('|');
		for(i = 0; i<entries; i++){
			a = entryLog.getEntry(i);
			//Serial3.print("Entry number: ");
			Serial3.print(i+1);
			Serial3.print(',');
			digitalClockDisplay(a.time);
			Serial3.print(',');
			Serial3.print(a.user,DEC);
			Serial3.print(',');
			Serial3.print(a.entryType,DEC);
			Serial3.print('\n');
		}
		Serial3.print('~');
		break;

	default:
		//Serial.println("Who sent that command?");
		break;
	}


	if(verboseon){
		Serial.print("dispLog took: ");
		Serial.print( sinceStart/1000 );
		Serial.println(" ms");
	}

}

void digitalClockDisplay(time_t t) {
	// digital clock display of the time

	switch(commandSentBy){
	case serial:
		printDigits(hour(t));
		Serial.print(":");
		printDigits(minute(t));
		Serial.print(":");
		printDigits(second(t));
		Serial.print(',');
		printDigits(month(t));
		Serial.print("/");
		printDigits(day(t));
		Serial.print("/");
		Serial.print(year(t));
		//	Serial.println();
		//Serial.write(10);
		break;
	case bluetooth:
		printDigits(hour(t));
		Serial2.print(":");
		printDigits(minute(t));
		Serial2.print(":");
		printDigits(second(t));
		Serial2.print(',');
		printDigits(month(t));
		Serial2.print("/");
		printDigits(day(t));
		Serial2.print("/");
		Serial2.print(year(t));
		//	Serial.println();
		//Serial.write(10);
		break;
	case wifi:
		printDigits(hour(t));
		Serial3.print(":");
		printDigits(minute(t));
		Serial3.print(":");
		printDigits(second(t));
		Serial3.print(',');
		printDigits(month(t));
		Serial3.print("/");
		printDigits(day(t));
		Serial3.print("/");
		Serial3.print(year(t));
		//	Serial.println();
		//Serial.write(10);
		break;
	default:
		break;
	}

}
void printDigits(int digits){
	// utility function for digital clock display: prints preceding colon and leading 0

	switch(commandSentBy){
	case serial:
		if(digits < 10)
			Serial.print('0');
		Serial.print(digits);
		break;
	case bluetooth:
		if(digits < 10)
			Serial2.print('0');
		Serial2.print(digits);
		break;
	case wifi:
		if(digits < 10)
			Serial3.print('0');
		Serial3.print(digits);
		break;
	default:
		break;
	}
}

time_t tmConvert_t(int YYYY, byte MM, byte DD, byte hh, byte mm, byte ss){
	tmElements_t tmSet;
	tmSet.Year = YYYY - 1970;
	tmSet.Month = MM;
	tmSet.Day = DD;
	tmSet.Hour = hh;
	tmSet.Minute = mm;
	tmSet.Second = ss;
	return makeTime(tmSet);         //convert to time_t
}

int extract(char *ret, char *arr, int pos){
	int maxEntryLength = 150; //if a value takes more than a specified length, the array is not changed
	int i = 0, j = 0, k = 0;
	while(pos > i){
		if(arr[j] == ','){
			i++;
		}
		j++;
		if(j>maxEntryLength){
			break;
		}
	}
	//Serial.print("index = ");
	//Serial.println(j);
	while( !(arr[j] == ',' || arr[j] == 0) ){
		if(j>maxEntryLength){
			break;
		}
		ret[k] = arr[j];
		k++;
		j++;
	}
	ret[k]=0;
	return k;
}

int extractall(char *ret, char *arr, int pos){
	int maxEntryLength = 50; //if a value takes more than a specified length, the array is not changed
	int i = 0, j = 0, k = 0;
	while(pos > i){
		if(arr[j] == ','){
			i++;
		}
		j++;
		if(j>maxEntryLength){
			break;
		}
	}
	//Serial.print("index = ");
	//Serial.println(j);
	while( arr[j] != 0 ){
		if(j>maxEntryLength){
			break;
		}
		ret[k] = arr[j];
		k++;
		j++;
	}
	ret[k] = 0;
	return k;
}

void chara2uinta(uint8_t *returnarr, char* inarr){
	int i = 0;
	while(inarr[i] != 0){
		returnarr[i] = (uint8_t)inarr[i];
		i++;
	}
}

void print( char *arr){
	//print until 0
	int i = 0;
	char temp = arr[0];
	while( temp!= 0){
		if(verboseon){
			Serial.print("0x");
			Serial.print(temp,HEX);
			Serial.print(" ");
		}
		if(verboseon){
			Serial.println(temp);
		}
		else{
			Serial.print(temp);
		}
		i++;
		temp = arr[i];
	}
	Serial.println("");
}

time_t getTeensy3Time(){
	return Teensy3Clock.get();
}

byte strCmp(char * arr, char * compto){
	int i = 0, j = 0;
	while( !(arr[i] == 0 || compto[i] == 0)  ){
		if (arr[i] != compto [i]){
			return 0;
		}
		i++;
	}
	if (arr[i] != compto [i]){
		return 0;
	}
	else
		return 1;
}

void lockDoor(){
	if(sinceLastLockEvent >5000){
		if(verboseon){
			Serial.println("Locking Door..");
		}
		if(doorStatus == open){
			Serial.println("Door is open");
			return;
		}
		digitalWrite(5,HIGH);
		//Alarm.timerOnce(1,led56Low);
		entryLog.logEntry(getTeensy3Time(),0,1);
		digitalWrite(16,HIGH);
		delay(55);
		digitalWrite(16,LOW);
		lockStatus = locked;
		sinceLastLockEvent = 0;
	}
}

void lockDoorNoLog(){
	if(sinceLastLockEvent >5000){
		if(verboseon){
			Serial.println("Locking Door (No Log)..");
		}
		if(doorStatus == open){
			Serial.println("Door is open");
			return;
		}
		digitalWrite(5,HIGH);
		//Alarm.timerOnce(1,led56Low);
		digitalWrite(16,HIGH);
		delay(55);
		digitalWrite(16,LOW);
		lockStatus = locked;
		sinceLastLockEvent = 0;
	}
}

void unlockDoor(int logId){
	if(sinceLastLockEvent >5000){
		if(doorStatus == open || lockStatus == unlocked){
			Serial.println("Door is open or unlocked");
			return;
		}
		if(verboseon){
			Serial.println("Unlocking door..");
		}
		digitalWrite(6,HIGH);
		Alarm.timerOnce(1,led56Low);
		digitalWrite(17,HIGH);
		delay(200);
		digitalWrite(17,LOW);
		delay(100);
		digitalWrite(15,HIGH);
		delay(200);
		digitalWrite(15,LOW);
		delay(100);
		digitalWrite(17,HIGH);
		delay(200);
		digitalWrite(17,LOW);
		delay(100);
		digitalWrite(15,HIGH);
		delay(200);
		digitalWrite(15,LOW);
		delay(100);
		lockStatus = unlocked;
		entryLog.logEntry(getTeensy3Time(),logId,0);

		sinceLastLockEvent = 0;
	}
}

//String removeExtraSpace(String input){ //Works but no longer needed
//
//	input.replace(13,10);
//	int tempInd10 = input.indexOf(10);
//	int length = input.length();
//
//	Serial.println(tempInd10);
//	Serial.println(length);
//
//
//
//	while( tempInd10 != -1 ){
//		if(tempInd10 == 0){
//			input = input.substring(1);
//
//			char temp[10];
//			input.toCharArray(temp,10);
//			print(temp);
//		}
//		else{
//			String t1 = input.substring(0,tempInd10);
//			String t2 = input.substring(tempInd10+1);
//			input = String(t1 + t2);
//
//			char temp[10];
//			input.toCharArray(temp,10);
//			print(temp);
//		}
//		if(tempInd10 == input.length()-1){
//			input.replace(10,0);
//			Serial.print("last");
//
//			char temp[10];
//			input.toCharArray(temp,10);
//			print(temp);
//		}
//
//		tempInd10 = input.indexOf(10);
//		Serial.println(tempInd10);
//		Serial.println(input.length());
//	}
//
//	return input;
//}

void getInput(){
	if(Serial.available()){
		//		Serial.readBytesUntil(10,receiverarray,150);
		receiverString = Serial.readStringUntil(10,150);//use string manip
		receiverString.toCharArray(receiverarray,receiverString.length()+1);
		//print(receiverarray);
		commandSentBy = serial;
	}
	else if(Serial2.available()){
		//Serial2.readBytesUntil(0xD,receiverarray,150);
		//print(receiverarray);
		receiverString = Serial2.readStringUntil(10,100);
		//rec = removeExtraSpace(rec);
		//Serial.println(receiverString); //remove to see printout
		receiverString.toCharArray(receiverarray,receiverString.length());
		//print(receiverarray);
		commandSentBy = bluetooth;
	}
	else if(Serial3.available()){
		Serial3.readBytesUntil(0xD,receiverarray,150);
		print(receiverarray);
		commandSentBy = wifi;
	}
	else{
		commandSentBy = -1;
	}
}

void clearArrays(){
	command_s = "";
	receiverString = "";
	memset(command,0,20);
	memset(receiverarray,0,150);
}

void printSender(){
	if(commandSentBy>0 && verboseon){
		digitalWrite(13,HIGH);
		Alarm.timerOnce(2,ledLow);

		switch(commandSentBy){
		case serial:
			Serial.println("Command sent by serial");
			break;
		case bluetooth:
			//Serial.println("Command sent by bluetooth");
			break;
		case wifi:
			Serial.println("Command sent by wifi");
			break;
		default:
			break;
		}
	}
}

void BTcommand(int command){
	switch(command){
	case at:
		Serial2.print("at");
		lastCommand = at;
		break;
	case atrssi:
		Serial2.print("atrssi?,0");
		lastCommand = atrssi;
		break;
	case cmode:
		Serial2.print("+++");
		lastCommand = cmode;
		break;
	case dmode:
		Serial2.print("atmd");
		lastCommand = dmode;
		break;
	case atdh:
		Serial2.print("atdh");
		lastCommand = atdh;//disconnect
		break;
	case atdc:
		Serial2.print("atdc");
		lastCommand = atdc;//cancel
		break;
	case atple:
		Serial2.print("atple,0");
		lastCommand = atple;
		break;

	default:
		break;
	}
	Serial2.write(0xd);
}

void processCommand(){
	command_s = getCommandString(receiverString);
	int sizee = extract(command,receiverarray, 0); //use size of command for switch

	if(sizee && verboseon){
		//Serial.print("Command size: ");
		//Serial.println(sizee);

	}

	if(command_s.equalsIgnoreCase("TESTING")){
		Serial.print("SUCCESS");
	}

	//	if(strCmp(command,"CONNECT")&&(commandSentBy==bluetooth)){//make another array for serial2
	//		BTconStatus = connected;
	//		clearArrays();
	//	}
	//	else if(strCmp(command,"DISCONNECT")&&(commandSentBy==bluetooth)){
	//		BTconStatus = disconnected;
	//		clearArrays();
	//	}
	//
	if(command_s.equalsIgnoreCase("CONNECT")&&(commandSentBy == bluetooth)){
		BTconStatus = connected;
		lastCommand = statusupdate_c;
		BTMode = dataMode;
		sinceConnect = 0;
		//currBTuser = "BTnoAPP";
		clearArrays();
		getInput();
		processCommand();
	}
	if(command_s.equalsIgnoreCase("DISCONNECT")&&(commandSentBy == bluetooth)){
		BTconStatus = disconnected;
		lastCommand = statusupdate_c;
		isPaired = false;
		BTMode = commandMode;
		distance.clearRSSI();
		currBTuser = "";
		clearArrays();
		//lockDoorNoLog();
	}
	if(strCmp(command,"BTID")){
		char temp[50];
		extractall(temp,receiverarray,1);
		currBTuser = temp;
		clearArrays();
	}
	if(strCmp(command,"settime")){
		char t[2];
		int yr;
		byte MM,DD,hh,mm,ss;
		extract(t,receiverarray,1);
		yr = 2000 + (int)((t[0]-48)*10) + (int)(t[1]-48);
		extract(t,receiverarray,2);
		MM = (int)((t[0]-48)*10) + (int)(t[1]-48);
		extract(t,receiverarray,3);
		DD = (int)((t[0]-48)*10) + (int)(t[1]-48);
		extract(t,receiverarray,4);
		hh = (int)((t[0]-48)*10) + (int)(t[1]-48);
		extract(t,receiverarray,5);
		mm = (int)((t[0]-48)*10) + (int)(t[1]-48);
		extract(t,receiverarray,6);
		ss = (int)((t[0]-48)*10) + (int)(t[1]-48);
		Teensy3Clock.set(tmConvert_t(yr,MM,DD,hh,mm,ss));
		lastCommand = settime_c;
		clearArrays();
	}
	if(strCmp(command,"disallow")){
		//container.disAllowUser(3);
		char temp[15];
		extract(temp,receiverarray,1);
		int logid = String(temp).toInt();
		container.disAllowUser(logid);
		clearArrays();
	}
	if(strCmp(command,"printuserinfo")){
		if(commandSentBy==serial){
			container.printUserInfoAll();
		}
		if(commandSentBy==wifi){
			container.printUserInfoAllWifi();
		}
		clearArrays();
	}

	if(strCmp(command,"unconfirmeduser?")){
		int i;
		if(commandSentBy==wifi){
			Serial3.print('|');
		}
		for(i =0; i<container.getNumUsers(); i++){
			String tmpUsrId = container.getUserId(i);
			if(tmpUsrId.startsWith("-")){
				//				Serial.print("Log Id: ");
				//				Serial.println(container.getLogId(i));
				//				Serial.print("User Id: ");
				//				Serial.println(container.getUserId(i));
				container.printUserInfo(i);
				if(commandSentBy==wifi){
					container.printUserInfoToWifi(i);
				}
			}
		}
		if(commandSentBy==wifi){
			Serial3.print('~');
		}
	}



	if(strCmp(command,"edituser")){
		char temp[30];
		extract(temp,receiverarray,1);
		int logid = String(temp).toInt();

		extract(temp,receiverarray,2);
		int MM1 = String(temp).toInt();
		extract(temp,receiverarray,3);
		int DD1 = String(temp).toInt();
		extract(temp,receiverarray,4);
		int YY1 = String(temp).toInt();
		extract(temp,receiverarray,5);
		int HH1 = String(temp).toInt();
		extract(temp,receiverarray,6);
		int mm1 = String(temp).toInt();
		extract(temp,receiverarray,7);
		int MM2 = String(temp).toInt();
		extract(temp,receiverarray,8);
		int DD2 = String(temp).toInt();
		extract(temp,receiverarray,9);
		int YY2 = String(temp).toInt();
		extract(temp,receiverarray,10);
		int HH2 = String(temp).toInt();
		extract(temp,receiverarray,11);
		int mm2 = String(temp).toInt();
		extract(temp,receiverarray,12);
		String alias = String(temp);
		time_t t1 = tmConvert_t(YY1+2000,MM1,DD1,HH1,mm1,0);
		time_t t2 = tmConvert_t(YY2+2000,MM2,DD2,HH2,mm2,0);

		container.editUser(logid,t1,t2,alias);

		clearArrays();
	}

	if(strCmp(command,"newuser")){
		char temp[30];
		extract(temp,receiverarray,1);
		String tempst = String(temp);
		if("keypad"){
			extract(temp,receiverarray,2);
			String pw = String(temp);

			extract(temp,receiverarray,3);
			int MM1 = String(temp).toInt();
			extract(temp,receiverarray,4);
			int DD1 = String(temp).toInt();
			extract(temp,receiverarray,5);
			int YY1 = String(temp).toInt();
			extract(temp,receiverarray,6);
			int HH1 = String(temp).toInt();
			extract(temp,receiverarray,7);
			int mm1 = String(temp).toInt();
			extract(temp,receiverarray,8);
			int MM2 = String(temp).toInt();
			extract(temp,receiverarray,9);
			int DD2 = String(temp).toInt();
			extract(temp,receiverarray,10);
			int YY2 = String(temp).toInt();
			extract(temp,receiverarray,11);
			int HH2 = String(temp).toInt();
			extract(temp,receiverarray,12);
			int mm2 = String(temp).toInt();
			extract(temp,receiverarray,13);
			String alias = String(temp);
			time_t t1 = tmConvert_t(YY1+2000,MM1,DD1,HH1,mm1,0);
			time_t t2 = tmConvert_t(YY2+2000,MM2,DD2,HH2,mm2,0);
			User tempuser(pw,container.getNumUsers(),t1,t2,alias);
			container.addUser(tempuser);
		}

		if("bluetooth"){
			pairNext = true;
		}
		clearArrays();
	}
	if(strCmp(command,"adjustautolockdelay")){
		char temp[30];
		extract(temp,receiverarray,1);
		int aldelay = String(temp).toInt();

		if(aldelay < 10 || aldelay>3600){
			autolockdelay = 30;
		}
		else{
			autolockdelay = aldelay;
		}
	}
	if(strCmp(command,"allowuser")){
		char temp[10];
		extract(temp,receiverarray,1);
		int logid = String(temp).toInt();

		container.allowBTuser(logid);
		clearArrays();
	}
	if(strCmp(command,"lognow")){
		Serial.println("lognow");
		//logEntry(getTeensy3Time(), 1,1);
		entryLog.logEntry(getTeensy3Time(), 1,1);
		clearArrays();
	}

	if(strCmp(command,"displaylog")){
		Serial.println("displaylog");
		dispLog();
		clearArrays();
	}
	if(strCmp(command,"clearlog")){
		entryLog.clearLog();
		container.clearDisallowed();
	}
	if(strCmp(command,"time?")){
		digitalClockDisplay(Teensy3Clock.get());
		clearArrays();
	}
	if(strCmp(command,"PAIRED")){
		isPaired = true;
		clearArrays();
	}
	if(strCmp(command,"BTstatus?")){
		if(BTconStatus == connected){
			Serial.println("BT is connected");
		}
		else{
			Serial.println("BT is Disconnected");
		}

		if(BTMode == dataMode){
			Serial.println("BT is in Data Mode");
		}
		else{
			Serial.println("BT is in Command Mode");
		}
		if(isPaired){
			Serial.println("BT is Paired");
		}
		else{
			Serial.println("BT is not Paired");
		}
		Serial.print("Current BT userID: ");
		Serial.println(currBTuser);
		//isPaired

		clearArrays();
	}
	if(strCmp(command,"pairnextBT")){
		pairNext = 1;
		clearArrays();
	}
	if(strCmp(command,"doorstatus?")){
		switch(commandSentBy){
		case serial:
			if(doorStatus == open){
				Serial.println("Door is open");
			}
			else{
				Serial.println("Door is closed");
			}

			if(lockStatus == locked){
				Serial.println("Door is locked");
			}
			else{
				Serial.println("Door is unlocked");
			}
			break;
		case bluetooth:
			if(doorStatus == open){
				Serial2.println("Door is open");
			}
			else{
				Serial2.println("Door is closed");
			}

			if(lockStatus == locked){
				Serial2.println("Door is locked");
			}
			else{
				Serial2.println("Door is unlocked");
			}
			break;
		case wifi:
			if(doorStatus == open){
				Serial3.println("Door is open");
			}
			else{
				Serial3.println("Door is closed");
			}

			if(lockStatus == locked){
				Serial3.println("Door is locked");
			}
			else{
				Serial3.println("Door is unlocked");
			}
			break;
		default:
			break;
		}
		//		if(doorStatus == open){
		//			Serial.println("Door is open");
		//		}
		//		else{
		//			Serial.println("Door is closed");
		//		}
		//
		//		if(lockStatus == locked){
		//			Serial.println("Door is locked");
		//		}
		//		else{
		//			Serial.println("Door is unlocked");
		//		}
	}

	if(strCmp(command,"BTsend")){
		char test[50];
		int size = extractall(test,receiverarray,1);
		Serial2.print(test);
		Serial2.write(0xd);
		clearArrays();
	}
	if(strCmp(command,"toggleLed")){
		digitalWrite(13,!digitalRead(13));
	}
	if(strCmp(command,"WIFIsend")){
		Serial.print("sending");
		char test[20];
		int size = extractall(test,receiverarray,1);
		uint8_t sendarr[size];
		chara2uinta(sendarr, test);
		Serial3.write('|');
		Serial3.write(sendarr,size);
		Serial3.write('~');
		clearArrays();
	}
	//	if(strCmp(command, "AT")){
	//
	//	}
	if(strCmp(command,"getRSSI")){
		if(BTMode != commandMode){
			BTcommand(atrssi);
		}

		clearArrays();
	}

	if(strCmp(command,"OK") && (commandSentBy == bluetooth)){
		//Serial.println("Bluetooth sent OK");//====================================================================
		if(lastCommand == cmode){
			BTMode = commandMode;
		}
		if(lastCommand == dmode){
			BTMode = dataMode;
		}
	}

	if(strCmp(command,"cmode")){
		//		Serial2.print("+++");
		//		Serial2.write(0xd);
		BTcommand(cmode);
		clearArrays();
	}
	if(strCmp(command,"dmode")){
		//		Serial2.print("atmd");
		//		Serial2.write(0xd);
		BTcommand(dmode);
		clearArrays();
	}
	if(strCmp(command,"doorlock")){
		lockDoor();
		clearArrays();
	}
	if(strCmp(command,"doorunlock")){
		switch(commandSentBy){
		case serial:
			unlockDoor(0);
			break;
		case bluetooth:
			checkAllowUnlock(currBTuser);
			break;
		case wifi:
			unlockDoor(1);
			break;
		default:
			break;
		}
		//		unlockDoor(0);
		clearArrays();
	}
	if(strCmp(command,"clear!")){
		clearArrays();
	}

	if(receiverarray[0]=='-'){
		int temp = 0;
		temp += (receiverarray[1]-48)*100;
		temp += (receiverarray[2]-48)*10;
		temp += (receiverarray[3]-48);
		Serial.print("RSSI: ");
		Serial.println(temp);
		distance.pushRSSI(temp);
	}

	clearArrays();
	delay(50);
}
void toggleDoorStatus(){//fix by adding in loop
	doorStatus = !doorStatus;
	if(doorStatus == open){
		Serial.println("dooropened");
		if(lockStatus==locked){
			lockStatus=unlocked;
		}
		entryLog.logEntry(getTeensy3Time(),0,2);
	}
	else if(doorStatus == closed){
		Serial.println("doorclosed");
		entryLog.logEntry(getTeensy3Time(),0,3);
	}
	//digitalWrite(13,doorStatus);
}

String getCommandString(String input){
	if(input.indexOf(',')==-1){
		return input;
	}
	else{
		return input.substring(0,input.indexOf(','));
	}
}
char getKey(){
	digitalWriteFast(0,LOW);
	digitalWriteFast(1,HIGH);

	if(digitalRead(2)==0){
		return 'B';
	}
	if(digitalRead(3)==0){
		return 'C';
	}
	if(digitalRead(4)==0){
		return 'D';
	}
	digitalWriteFast(0,HIGH);
	digitalWriteFast(1,LOW);
	if(digitalRead(2)==0){
		return 'F';
	}
	if(digitalRead(3)==0){
		return 'E';
	}
	if(digitalRead(4)==0){
		return 'A';
	}
	return 0;
}
void redOn(){
	analogWrite(23,128);
}
void greenOn(){
	analogWrite(22,200);
}
void redOff(){
	analogWrite(23,0);
}
void greenOff(){
	analogWrite(22,0);
}
void orangeOn(){
	redOn();
	greenOn();
}
void orangeOff(){
	redOff();
	greenOff();
}
void led56Low(){
	digitalWrite(5,LOW);
	digitalWrite(6,LOW);
}
void checkAllowUnlock(String entered){
	int i,found=0;
	Serial.print("Checking for ");
	Serial.println(entered);
	for(i = 0;i<container.getNumUsers();i++){
//
//		Serial.print("Checking User ");
//		Serial.print(i);
//		Serial.print(", ");
//		Serial.print("User Code is ");
//		Serial.println(container.getUserId(i));

		if(entered.equals(container.getUserId(i))  && container.isAllowed(i)){
			//commandSentBy = keypad;
			found=1;
			unlockDoor(container.getLogId(i));
			Serial.print("Log ID was ");
			Serial.print(container.getLogId(i));
			Serial.print(", ");
			Serial.print("Alias: ");
			Serial.println(container.getAlias(i));
		}
//		if(!found){
//			Serial.print("Did not find allowed user");
//		}
	}
}
void updateConnectionMode(){
	if(digitalRead(11)){
		connectionMode = false; //connectionMode means will get rssi to open door
	}
	else{
		connectionMode = true;
	}
}
void bufferedGetKey(){
	char keyPressed = getKey();
	if(keyPressed != prevKey){
		prevKey = keyPressed;
		if(keyPressed){
			sinceLastKeyPress = 0;
			Serial.print("Key Pressed: ");
			Serial.println(getKey());
			orangeOn();
			delay(150);
			orangeOff();
			if(keyPressed != 'F'){ //need both == and != becaause of 0 reutrn
				keyPressArray[keysEntered]=keyPressed;
				keysEntered++;
				Serial.print("Keys currently entered: ");
				Serial.println(keyPressArray);
				if(keysEntered > 10){ //max keypad password size 10
					checkAllowUnlock(keyPressArray);
					keysEntered = 0;
					memset(keyPressArray,0,11);
				}
			}
			else if(keyPressed == 'F'){
				if(lockStatus == unlocked){
					lockDoor();
					return;
				}
				checkAllowUnlock(keyPressArray);
				//checkAllowUnlock(currBTuser);
				keysEntered = 0;
				memset(keyPressArray,0,11);
			}
		}
	}
	if(sinceLastKeyPress > 10000 && keysEntered>0){
		keysEntered = 0;
		memset(keyPressArray,0,11);
	}
}

void autolock(){
	if(autoLockEnable){
		if(sinceLastAutoLock > (autolockdelay*1000) ){
			lockDoorNoLog();
			sinceLastAutoLock = 0;
		}
	}
}


