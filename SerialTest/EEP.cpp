/*
 * EEP.cpp
 *
 *  Created on: Feb 28, 2014
 *      Author: Computer
 */

#include "EEP.h"
#include "Entry.h"
#include <Wire.h>

EEP::EEP(uint8_t address)
:_eeAddress(address)
{
	// TODO Auto-generated constructor stub
	//eeAddress = address;
}

void EEP::setAddress(unsigned int address){
	Wire.beginTransmission(_eeAddress);
	Wire.send((int)(address >> 8));   // MSB
	Wire.send((int)(address & 0xFF)); // LSB
	Wire.endTransmission();
}

void EEP::write(unsigned int eePosition, byte data){
	Wire.beginTransmission(_eeAddress);
	Wire.send((int)(eePosition >> 8));   // MSB
	Wire.send((int)(eePosition & 0xFF)); // LSB
	Wire.send(data);
	Wire.endTransmission();
	delay(5);
}

Entry EEP::readEntry(int pos){
	unsigned int entryStart = (5*pos)+2;
	setAddress(entryStart);
	Wire.requestFrom((int)_eeAddress,5);
	byte arr[5];
	int index=0;
	while(Wire.available()){
		arr[index]=Wire.receive();
		index++;
	}
	uint32_t byte0 = (uint32_t)arr[0];
	uint32_t byte1 = (uint32_t)arr[1];
	uint32_t byte2 = (uint32_t)arr[2];
	uint32_t byte3 = (uint32_t)arr[3];
	time_t ret_t = (time_t)(byte0<<24 | byte1<<16 | byte2<<8 | byte3);
	Entry a = {ret_t, arr[4]>>2, arr[4]&0x03};
	return a;
}


EEP::~EEP() {
	// TODO Auto-generated destructor stub
}

