#include <Time.h>  
#include <Wire.h>


time_t tmConvert_t(int YYYY, byte MM, byte DD, byte hh, byte mm, byte ss);
time_t getTeensy3Time();
void digitalClockDisplay();

boolean compare(char a[], char b[]){
  unsigned char j=0;
  if(sizeof(a) != sizeof(b)){
    return false;
  }
  for(j = 0; j<sizeof(a); j++){
    if(!(a[j] == b[j])){
      return false;
    }
  }
  return true;
}

void printstr(char input[]){
  unsigned char j = 0;
  for(j; j<sizeof(input)-1;j++){
    Serial.print(input[j]);
  }
  Serial.print('\n');
}

void S1_receive(char arr[]){
  unsigned char index = 0;
  char incomingbyte = Serial1.read();
  while(incomingbyte != 0xD){
    arr[index] = incomingbyte;
    index++;
    incomingbyte = Serial1.read();
    Serial.print(index);
  }
  arr[index]=0;
  
  
}

void setup() {
  setSyncProvider(getTeensy3Time);
  //Teensy3Clock.set(tmConvert_t(2013,11,15,6,49,40));
  Serial.begin(115200);
  Serial1.begin(115200);
  Wire.begin();
  pinMode(11,OUTPUT);
  pinMode(12,OUTPUT);
  pinMode(13,OUTPUT);
  digitalWrite(13,HIGH);
  pinMode(14,INPUT);
  digitalWrite(11,LOW);
  digitalWrite(12,HIGH);
}

char incomingArray[20];

char open[]="open";
char close[]="close";
char teststr[]="this is a test";

void loop() {
  unsigned char i;
  unsigned char incomingByte;
  if(Serial.available()){
    while (Serial.available() > 0) {
      incomingByte = Serial.read();
      Serial1.write(incomingByte);
    }
    Serial1.write(0xD);
  }

  i=0;
  if (Serial1.available() > 0) {
    while(Serial1.available() > 0){
      incomingByte = Serial1.read();
      if(incomingByte == 0xD){
        incomingByte = 0;
      }
      incomingArray[i] = incomingByte;
      i++;
      Serial.print("0x");
      Serial.print(incomingByte,HEX);
      Serial.print("   ");
      Serial.write(incomingByte);
      Serial.print('\n');
    }
      //S1_receive(incomingArray);
      //Serial.println(incomingArray);
      //Serial.print(sizeof(teststr));
      //Serial.print('\n');
    digitalClockDisplay();
  }

  if( compare(open,incomingArray) ){
    digitalWrite(11,HIGH);
    digitalWrite(12,LOW);
  }
  if( compare(close,incomingArray) ){
    digitalWrite(11,LOW);
    digitalWrite(12,HIGH);
  }
}




void printDigits(int digits){
  // utility function for digital clock display: prints preceding colon and leading 0
  Serial.print(":");
  if(digits < 10)
    Serial.print('0');
  Serial.print(digits);
}
