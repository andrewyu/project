// Do not remove the include below
#include "serial.h"

uint8_t numbytes;
char incomingArray[60];
int count(char *arr);

//The setup function is called once at startup of the sketch
void setup()
{
// Add your initialization code here
	Serial.begin(9600);
}

// The loop function is called in an endless loop
void loop()
{
//Add your repeated code here
	if(Serial.available()){
		numbytes = Serial.readBytesUntil('A', incomingArray, 60);
		Serial.print(count(incomingArray));
	}
}

int count(char *arr){
	uint8_t temp = 0;
	while(arr[temp] != 'A'){
		temp++;
	}
	return temp;
}
