/*
 * doorLock.h
 *
 *  Created on: Apr 3, 2014
 *      Author: Computer
 */

#ifndef DOORLOCK_H_
#define DOORLOCK_H_

class doorLock {
public:
	doorLock();
	virtual ~doorLock();
private:
	int _doorStatus;
	int _lockStatus;

};

#endif /* DOORLOCK_H_ */
