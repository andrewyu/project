# README #

This repository contains the files used in our EE175 Senior Design Project.

### Mobile Phone Controlled Lock and Access Control  ###

* Project by Andrew Yu, Kenneth Cai, and Richard Dinh
* Our project is a door lock that is wifi and bluetooth enabled. The door will automatically unlock in the presence of an authorized bluetooth 4.0 enabled iPhone. Access control settings are set and changed via the Android app through wifi.
* Version 1

### Contents ###
* SeniorDesignFinal contains the code loaded on the microcontroller at the time of presentation
* EE175 Xcode files.zip contains files for the iPhone app 
* Other directories are of code used to test various parts of the system

### Contribution guidelines ###

We want to thank the writers of the Arduino IDE and the writers of the libraries packaged with the IDE. We would also like to thank the writers of the sample application nBlueTerm, from BlueRadios. 

### Who do I talk to? ###

* For any issues, please contact yuandrew08@gmail.com