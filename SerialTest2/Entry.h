#ifndef Entry_H_
#define Entry_H_

#include <arduino.h>
typedef struct Entry{
	time_t time;
	byte user;
	byte entryType;
} Entry;

#endif
